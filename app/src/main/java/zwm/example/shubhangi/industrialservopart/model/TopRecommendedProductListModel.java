package zwm.example.shubhangi.industrialservopart.model;

/**
 * Created by shubhangi on 1/4/2018.
 */

public class TopRecommendedProductListModel {
    private String ID;
    private String ebay_id;
    private String title;
    private String image;
    private String price;
    private String status;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getEbay_id() {
        return ebay_id;
    }

    public void setEbay_id(String ebay_id) {
        this.ebay_id = ebay_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



}
