package zwm.example.shubhangi.industrialservopart.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.activity.BaseActivity;
import zwm.example.shubhangi.industrialservopart.activity.EbaySubProductDetailDescriptionActivity;
import zwm.example.shubhangi.industrialservopart.model.CartItemModel;
import zwm.example.shubhangi.industrialservopart.util.CartUpdateListener;
import zwm.example.shubhangi.industrialservopart.util.MyApplication;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.util.UserData;

/**
 * Created by shubhangi on 1/16/2018.
 */

public class CartItemAdapter extends RecyclerView.Adapter<CartItemAdapter.ViewHolder> {
    int i;
    List<CartItemModel> cartItemModelList;
    CartItemModel cartItemModel;
    private Context context;

    private CartUpdateListener cartUpdateObserver;

    public void addObsever(CartUpdateListener cartUpdateListener) {
        this.cartUpdateObserver = cartUpdateListener;
    }

    public void notifyObserver(String cartValue) {
        if (null != cartUpdateObserver) {
            cartUpdateObserver.updateCartValue(cartValue);
        }
    }


    // data is passed into the constructor
    public CartItemAdapter(Context context, List<CartItemModel> cartItemModelList) {
        super();
        this.context = context;
        this.cartItemModelList = cartItemModelList;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_cart_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        cartItemModel = cartItemModelList.get(position);
        holder.productNameInCart.setText(cartItemModel.getCart_item_title());
        holder.productNameInCart.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        holder.productPriceInCart.setText("$" + cartItemModel.getCart_item_price() + " x " + cartItemModel.getCart_item_quantity() +
                " = " + "$" + cartItemModel.getCart_item_updated_price());
        holder.productQuantityInCart.setText("Quantity - ");
        //cartItemModelList.get(position).setProduct_qty_in_cart_item(cartItemModel.getProduct_qty_cart());
        holder.number.setText(cartItemModel.getCart_item_quantity());
        Glide.with(context)
                .load(cartItemModel.getCart_item_image())
                .into(holder.imageViewProductInCart);
    }


    // total number of cells
    @Override
    public int getItemCount() {
        return cartItemModelList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageViewProductInCart, imageCloseIcon;
        public TextView productNameInCart, productPriceInCart, productQuantityInCart, number;
        public ImageButton plusButton, minusButton;

        public ViewHolder(final View itemView) {
            super(itemView);

            imageViewProductInCart = itemView.findViewById(R.id.imageViewProductInCart);
            imageCloseIcon = itemView.findViewById(R.id.imageCloseIcon);
            productNameInCart = itemView.findViewById(R.id.productNameInCart);
            productPriceInCart = itemView.findViewById(R.id.productPriceInCart);
            productQuantityInCart = itemView.findViewById(R.id.productQuantityInCart);

            minusButton = itemView.findViewById(R.id.minusButton);
            plusButton = itemView.findViewById(R.id.plusButton);
            number = itemView.findViewById(R.id.number);

            imageCloseIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((BaseActivity) context).showProgressDialog("Please Wait..");
                    StringRequest stringRequestNew = new StringRequest(com.android.volley.Request.Method.POST, Urls.REMOVE_FROM_CART,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("TAG", "response" + response);
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                            Toast.makeText(context, "Item removed successfully from your cart", Toast.LENGTH_SHORT).show();
                                            JSONObject getJsonObject = jsonObject.getJSONObject("response");
                                            UserData.CART_TOTAL_BILL = getJsonObject.getString("total");
                                            notifyObserver(getJsonObject.getString("total"));
                                            Log.e("UPdate ",getJsonObject.getString("total"));
                                            BaseActivity activity = (BaseActivity) context;
                                            BaseActivity.finalCartCount = getJsonObject.getInt("count_product");
                                            activity.refreshCartCounter();

                                        }
                                        else{
                                            notifyObserver("0");
                                            BaseActivity activity = (BaseActivity) context;
                                            BaseActivity.finalCartCount = 0;
                                            activity.refreshCartCounter();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Log.e("TAG", "ERROR_LOGIN: " + e.getMessage());
                                    }
                                   /* Toast.makeText(context, "Item removed successfully from your cart", Toast.LENGTH_SHORT).show();
                                    cartItemModelList.remove(getLayoutPosition());
                                    notifyItemRemoved(getLayoutPosition());
                                    notifyItemRangeChanged(getLayoutPosition(), cartItemModelList.size());


                                    try {
                                        JSONObject responseObj = new JSONObject(response);
                                        JSONObject jsonO = responseObj.getJSONObject("response");
                                        UserData.CART_TOTAL_BILL = jsonO.getString("total");
                                        Log.d("TAG", UserData.CART_TOTAL_BILL);
                                        *//*BaseActivity activity = (BaseActivity) context;
                                        activity.finalCartCount = responseObj.getInt("totalitems");
                                        activity.refreshCartCounter();*//*
                                    } catch (JSONException e) {
                                        e.printStackTrace();

                                    }*/
                                    cartItemModelList.remove(getLayoutPosition());
                                    notifyItemRemoved(getLayoutPosition());
                                    notifyItemRangeChanged(getLayoutPosition(), cartItemModelList.size());
                                    ((BaseActivity) context).dismissProgressDialog();
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                                    ((BaseActivity) context).dismissProgressDialog();
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();
                            params.put("user_id", UserData.USER_ID);
                            params.put("product_id", cartItemModelList.get(getLayoutPosition()).getCart_item_id());
                            Log.d("TAG", "params" + params);
                            return params;

                        }
                    };
                    MyApplication.getInstance().addToRequestQueue(stringRequestNew);
                }
            });


            minusButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    int x = Integer.parseInt(number.getText().toString());
                    x--;
                    if (x < 1) {
                        x = 1;
                    }
                    number.setText(String.valueOf(x));
                    cartItemModelList.get(getLayoutPosition()).setCart_item_quantity(String.valueOf(x));
                }
            });
            plusButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int x = Integer.parseInt(number.getText().toString());
                    x++;
                    number.setText(String.valueOf(x));
                    cartItemModelList.get(getLayoutPosition()).setCart_item_quantity(String.valueOf(x));
                }
            });

            productNameInCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent in = new Intent(context, EbaySubProductDetailDescriptionActivity.class);
                    Bundle b = new Bundle();
                    b.putString("ebayId", cartItemModelList.get(getLayoutPosition()).getCart_item_SKU_number());
                    b.putString("ebaySubProductID", cartItemModelList.get(getAdapterPosition()).getCart_item_id());
                    Log.d("TAG", "ebayId" + cartItemModelList.get(getLayoutPosition()).getCart_item_SKU_number());
                    in.putExtras(b);
                    context.startActivity(in);
                }
            });

        }
    }

}





