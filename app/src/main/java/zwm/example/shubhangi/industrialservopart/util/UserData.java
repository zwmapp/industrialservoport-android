package zwm.example.shubhangi.industrialservopart.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by shubhangi on 1/2/2018.
 */

public class UserData {
    public static String USER_ID,USER_FULL_NAME,USER_EMAIL,USER_PHONE,USER_ADDRESS,USER_USERNAME,
    SERVO_MOTOR_REPAIR_BUTTON,INDUSTRIAL_MONITORS_REPAIR_BUTTON,ALPHABET_VALUE,BRAND_VALUE_FOR_ALPHABET_MANUFACTURER,
            CART_TOTAL_BILL, CT_VALUE,
    PRODUCT_ID_IN_CART,
            PAYMENT_TXN_ID,
            PAYMENT_STATUS;


    public static Boolean LOGGED_IN;

    static {
        USER_ID = USER_FULL_NAME = USER_EMAIL = USER_PHONE = USER_ADDRESS = USER_USERNAME = SERVO_MOTOR_REPAIR_BUTTON =
                INDUSTRIAL_MONITORS_REPAIR_BUTTON = ALPHABET_VALUE = BRAND_VALUE_FOR_ALPHABET_MANUFACTURER =
                        CART_TOTAL_BILL = PRODUCT_ID_IN_CART = PAYMENT_TXN_ID = PAYMENT_STATUS  = CT_VALUE  ="";
        LOGGED_IN = false;
    }
    public static void getUserDataFromPreference(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("UserPref", 0);
        USER_ID = prefs.getString("key_userid", "");
        USER_FULL_NAME = prefs.getString("key_userfullname", "");
        USER_USERNAME = prefs.getString("key_userUsername","");
        USER_EMAIL = prefs.getString("key_email", "");
        USER_PHONE = prefs.getString("key_phone","");
        USER_ADDRESS = prefs.getString("key_address","");

        LOGGED_IN = prefs.getBoolean("key_loggedIn", false);

    }
    public static void clearUserData(Context context) {
       /* try {
            LoginManager.getInstance().logOut();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        SharedPreferences preferences = context.getSharedPreferences("UserPref", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
        USER_ID = USER_FULL_NAME  = USER_EMAIL = "";

        LOGGED_IN = false;

    }
}
