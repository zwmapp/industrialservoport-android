package zwm.example.shubhangi.industrialservopart.util;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Patterns;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Created by shubhangi on 12/5/2017.
 */

public class CommonApi {
    private static CommonApi commonApiInstance = null;

    @SuppressWarnings("unused")
    private final static String TAG = CommonApi.class.getCanonicalName();
    public static Vector<Activity> activityStore = new Vector<Activity>();

    public Activity activity;

    /**
     * Activity List.
     */
    public List<Activity> activityClass = new ArrayList<Activity>();


    protected CommonApi() {
        // Exists only to defeat instantiation.
    }

    public String errorMsg = "";

    public static CommonApi getInstance(Activity activity) {
        if (commonApiInstance == null) {
            commonApiInstance = new CommonApi();
            activityStore.addElement(activity);
        }
        commonApiInstance.activity = activity;
        return commonApiInstance;
    }

    public void openNewScreen(Class<?> cls, Bundle bundle) {
        Intent intent = new Intent(
                commonApiInstance.activity.getApplicationContext(), cls);
        if (bundle != null)
            intent.putExtras(bundle);
        commonApiInstance.activity.startActivity(intent);
        commonApiInstance.activity.overridePendingTransition(
                android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }




    public static void registerActivity(Activity context) {
        activityStore.addElement(context);
    }

    public static Vector<Activity> getAllAcivities() {
        return activityStore;
    }

    @SuppressWarnings("deprecation")
    public static void removeAllActivities() {
        for (int ii = 0; ii < activityStore.size(); ii++) {
            (activityStore.elementAt(ii)).finish();
        }

        System.runFinalizersOnExit(true);
        activityStore.removeAllElements();
    }
    public String checkRequiredFields(ViewGroup viewGroup) {
        int count = viewGroup.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = viewGroup.getChildAt(i);
            if (view instanceof ViewGroup)
                checkRequiredFields((ViewGroup) view);

            else if (view instanceof EditText) {
                EditText edittext = (EditText) view;
                if (edittext.getText().toString().trim().equals("")) {
//                    android.support.design.widget.TextInputLayout parent = (android.support.design.widget.TextInputLayout) edittext.getParent();
                    if (errorMsg.equals(""))
                        errorMsg = "" + edittext.getTag();
                }
            }
        }
        return errorMsg;
    }
    public static String splitString(String myName) {
        if(myName.equals("")){
            return "";
        }
        myName = myName.toUpperCase();
        String temp = "";
        String[] myNameArray = myName.split(" ");
        for (int i = 0; i < myNameArray.length; i++) {
            temp = temp + myNameArray[i].charAt(0);
        }
        return (temp + ".");
    }
    public void showSnackBar(View root_layout, String message, int length) {
        if (root_layout instanceof ViewGroup) {
            if (length == 0) {
                Snackbar.make(root_layout, message, Snackbar.LENGTH_SHORT).show();
            } else if (length == 1) {
                Snackbar.make(root_layout, message, Snackbar.LENGTH_LONG).show();
            } else {
                Snackbar.make(root_layout, message, Snackbar.LENGTH_INDEFINITE).show();
            }
        } else {

        }
    }

    // email vaildation
    public boolean checkEmailValidation(String text) {
        if (!(text.length() > 0)) {
            // showToastMsg("Please enter your email Id");
            return false;
        } else {
            boolean result = Patterns.EMAIL_ADDRESS.matcher(text).matches();
            if (!result) {
                //   showToastMsg("Please enter valid email id");
            }
            return result;
        }
    }
    //email vaildation

    //phone validation
    public boolean checkMobileNumber(String text) {
        if (!(text.length() > 0)) {
            // showToastMsg("Please enter your Phone number");
            return false;
        } else {
            boolean result = Patterns.PHONE.matcher(text).matches();
            if (!result) {
                //     showToastMsg("Please enter valid mobile number");
            }
            return result;
        }
    }
    //phone validation
    public void clearForm(ViewGroup group) {
        for (int i = 0, count = group.getChildCount(); i < count; ++i) {
            View view = group.getChildAt(i);
            if (view instanceof EditText) {
                ((EditText) view).setText("");
            }

            if (view instanceof ViewGroup && (((ViewGroup) view).getChildCount() > 0))
                clearForm((ViewGroup) view);
        }
    }

}
