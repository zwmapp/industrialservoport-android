package zwm.example.shubhangi.industrialservopart.model;

/**
 * Created by shubhangi on 1/5/2018.
 */

public class EbaySubProductModel {
    private String ebay_sub_product_product_id;
    private String ebay_id;
    private String ebay_sub_product_product_title;
    private String ebay_sub_product_product_image;
    private String ebay_sub_product_product_price;
    private String ebay_sub_product_product_status;

    public String getEbay_sub_product_product_id() {
        return ebay_sub_product_product_id;
    }

    public void setEbay_sub_product_product_id(String ebay_sub_product_product_id) {
        this.ebay_sub_product_product_id = ebay_sub_product_product_id;
    }

    public String getEbay_id() {
        return ebay_id;
    }

    public void setEbay_id(String ebay_id) {
        this.ebay_id = ebay_id;
    }

    public String getEbay_sub_product_product_title() {
        return ebay_sub_product_product_title;
    }

    public void setEbay_sub_product_product_title(String ebay_sub_product_product_title) {
        this.ebay_sub_product_product_title = ebay_sub_product_product_title;
    }

    public String getEbay_sub_product_product_image() {
        return ebay_sub_product_product_image;
    }

    public void setEbay_sub_product_product_image(String ebay_sub_product_product_image) {
        this.ebay_sub_product_product_image = ebay_sub_product_product_image;
    }

    public String getEbay_sub_product_product_price() {
        return ebay_sub_product_product_price;
    }

    public void setEbay_sub_product_product_price(String ebay_sub_product_product_price) {
        this.ebay_sub_product_product_price = ebay_sub_product_product_price;
    }

    public String getEbay_sub_product_product_status() {
        return ebay_sub_product_product_status;
    }

    public void setEbay_sub_product_product_status(String ebay_sub_product_product_status) {
        this.ebay_sub_product_product_status = ebay_sub_product_product_status;
    }



}
