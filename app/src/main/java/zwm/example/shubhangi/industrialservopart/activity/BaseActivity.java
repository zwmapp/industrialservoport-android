package zwm.example.shubhangi.industrialservopart.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.alertdialog.DialogBoxFactory;
import zwm.example.shubhangi.industrialservopart.util.CommonApi;
import zwm.example.shubhangi.industrialservopart.util.UserData;

/**
 * Created by shubhangi on 12/5/2017.
 */

public class BaseActivity extends AppCompatActivity {
    public String TAG = BaseActivity.class.getCanonicalName();
    public CommonApi commonApi;
    private ProgressDialog progressDialog;

    public String errorMsg = "";
    public TextView counterView;
    public ImageView imageViewCart;

    public static int finalCartCount = 0;


    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        commonApi = CommonApi.getInstance(BaseActivity.this);

        refreshCartCounter();

    }

    public void showProgressDialog(String message) {
        progressDialog = new ProgressDialog(this, R.style.MyAlertDialogStyle);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public void dismissProgressDialog() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        MenuItem cartItem = menu.getItem(0);
        cartItem.setActionView(R.layout.cart_badge);

        counterView = (TextView) cartItem.getActionView().findViewById(R.id.itemCounterOnCart);
        refreshCartCounter();

        imageViewCart = (ImageView) cartItem.getActionView().findViewById(R.id.imageViewCart);
        imageViewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //commonApi.openNewScreen(CartItemActivity.class, null);
                if (!UserData.LOGGED_IN) {
                    new DialogBoxFactory("Error!!", "Sorry, you are not logged in. Please log in to continue.", BaseActivity.this, DialogBoxFactory.TYPE_SINGLE_BUTTON, "OK") {
                        @Override
                        public void positiveClick(DialogInterface dialog, int id) {
//                            startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                            commonApi.openNewScreen(LoginActivity.class, null);
                            dialog.dismiss();
                        }

                        @Override
                        public void negativeClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    }.call();
                }
                else {
                    commonApi.openNewScreen(CartItemActivity.class, null);
                }
            }
        });

        return true;
    }
    public void refreshCartCounter() {
        if(null != counterView){
            counterView.setText(String.valueOf(finalCartCount));
            Log.d(TAG, "finalCartCount" + finalCartCount);
        }
    }

}




