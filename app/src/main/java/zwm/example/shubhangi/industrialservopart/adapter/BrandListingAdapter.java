package zwm.example.shubhangi.industrialservopart.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import me.drakeet.materialdialog.MaterialDialog;
import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.activity.BaseActivity;
import zwm.example.shubhangi.industrialservopart.activity.BrandManufacturerActivity;
import zwm.example.shubhangi.industrialservopart.model.BrandListingModel;
import zwm.example.shubhangi.industrialservopart.util.CommonApi;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.util.UserData;

/**
 * Created by shubhangi on 1/8/2018.
 */

public class BrandListingAdapter extends RecyclerView.Adapter<BrandListingAdapter.ViewHolder> {

    List<BrandListingModel> brandListingModelList;
    BrandListingModel brandListingModel;
    private Context context;

    MaterialDialog materialDialog;
    private ItemClickListener mClickListener;


    // data is passed into the constructor
    public BrandListingAdapter(Context context, List<BrandListingModel> brandListingModelList) {
        super();
        this.context = context;
        this.brandListingModelList = brandListingModelList;
    }

    // inflates the cell layout from xml when needed
    @Override
    public BrandListingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.brand_listing_recyclerview_items, parent, false);
        return new BrandListingAdapter.ViewHolder(view);
    }

    // binds the data to the textView in each cell
    @Override
    public void onBindViewHolder(BrandListingAdapter.ViewHolder holder, final int position) {
        brandListingModel = brandListingModelList.get(position);

        holder.brandTitle.setText(brandListingModel.getBrand_title());
        Glide.with(context)
                .load(Urls.BASE_URL_FOR_BRAND_IMAGE + brandListingModel.getBrand_image())
                .into(holder.brandImage);
        /*holder.brandProductDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View view1 = LayoutInflater.from(context).inflate(R.layout.brand_product_description_dialog, null);
                materialDialog = new MaterialDialog(context).setContentView(view1);
                TextView textProductDescription = view1.findViewById(R.id.textProductDescription);
                textProductDescription.setText(Html.fromHtml(brandListingModelList.get(position).getBrand_description()));

                ImageView imageCloseIcon = view1.findViewById(R.id.imageCloseIcon);
                imageCloseIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        materialDialog.dismiss();
                    }
                });
                materialDialog.show();

            }
        });*/
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return brandListingModelList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView brandTitle,buttonViewManufacturer;
        public ImageView brandImage;
       /* public Button brandProductDescription;*/

        public ViewHolder(View itemView) {
            super(itemView);
            brandTitle = itemView.findViewById(R.id.brandTitle);
            brandImage = itemView.findViewById(R.id.brandImage);
           /* brandProductDescription = itemView.findViewById(R.id.brandProductDescription);*/
            buttonViewManufacturer = itemView.findViewById(R.id.buttonViewManufacturer);

            buttonViewManufacturer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mClickListener != null)
                        mClickListener.onItemClick(view, getAdapterPosition());
                    Intent in = new Intent(context, BrandManufacturerActivity.class);
                    in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    Bundle b = new Bundle();
                    b.putInt("brandId", Integer.parseInt(brandListingModelList.get(getAdapterPosition()).getBarnd_id()));
                    UserData.BRAND_VALUE_FOR_ALPHABET_MANUFACTURER = brandListingModelList.get(getAdapterPosition()).getBarnd_id();
                    Log.d("TAG","BRAND_VALUE_FOR_ALPHABET_MANUFACTURER" +UserData.BRAND_VALUE_FOR_ALPHABET_MANUFACTURER);
                    in.putExtras(b);
                    context.startActivity(in);
                }
            });

        }
    }
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}





