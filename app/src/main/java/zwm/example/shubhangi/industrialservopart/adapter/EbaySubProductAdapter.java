package zwm.example.shubhangi.industrialservopart.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.activity.BaseActivity;
import zwm.example.shubhangi.industrialservopart.activity.CartItemActivity;
import zwm.example.shubhangi.industrialservopart.activity.EbaySubProductDetailDescriptionActivity;
import zwm.example.shubhangi.industrialservopart.model.EbaySubProductModel;
import zwm.example.shubhangi.industrialservopart.model.ServicesModel;
import zwm.example.shubhangi.industrialservopart.util.MyApplication;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.util.UserData;

/**
 * Created by shubhangi on 1/5/2018.
 */

public class EbaySubProductAdapter extends RecyclerView.Adapter<EbaySubProductAdapter.ViewHolder> {

    List<EbaySubProductModel> ebaySubProductModelList;
    EbaySubProductModel ebaySubProductModel;
    private Context context;

    // data is passed into the constructor
    public EbaySubProductAdapter(Context context, List<EbaySubProductModel> ebaySubProductModelList) {
        super();
        this.context = context;
        this.ebaySubProductModelList = ebaySubProductModelList;
    }

    // inflates the cell layout from xml when needed
    @Override
    public EbaySubProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ebay_sub_products_recyclerview_items, parent, false);
        return new EbaySubProductAdapter.ViewHolder(view);
    }

    // binds the data to the textView in each cell
    @Override
    public void onBindViewHolder(EbaySubProductAdapter.ViewHolder holder, final int position) {
        ebaySubProductModel = ebaySubProductModelList.get(position);

        holder.ebaySubProductTitle.setText(ebaySubProductModel.getEbay_sub_product_product_title());
        holder.ebaySubProductPrice.setText("$" + ebaySubProductModel.getEbay_sub_product_product_price());
        Glide.with(context)
                .load(ebaySubProductModel.getEbay_sub_product_product_image())
                .into(holder.ebaySubProductImage);
        holder.buttonAddtoCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(UserData.LOGGED_IN){
                    addToCartMethod(ebaySubProductModelList.get(position).getEbay_sub_product_product_id());
                }
                else
                {
                    Toast.makeText(context, "Please log in to add item in cart.", Toast.LENGTH_SHORT).show();
                }
                /*addToCartMethod(ebaySubProductModelList.get(getAdapterPosition()).getEbay_sub_product_product_id());
                //ebaySubProductModelList.get(getAdapterPosition()).getEbay_sub_product_product_id();*/

            }
        });
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return ebaySubProductModelList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView ebaySubProductTitle, ebaySubProductPrice;
        public ImageView ebaySubProductImage;
        public Button ebaySubProductView, buttonAddtoCart;

        public ViewHolder(View itemView) {
            super(itemView);
            ebaySubProductTitle = itemView.findViewById(R.id.ebaySubProductTitle);
            ebaySubProductPrice = itemView.findViewById(R.id.ebaySubProductPrice);
            ebaySubProductImage = itemView.findViewById(R.id.ebaySubProductImage);
            ebaySubProductView = itemView.findViewById(R.id.ebaySubProductView);
            buttonAddtoCart = itemView.findViewById(R.id.buttonAddtoCart);
            ebaySubProductView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent in = new Intent(context, EbaySubProductDetailDescriptionActivity.class);
                    in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    Bundle b = new Bundle();
                    b.putString("ebaySubProductID",ebaySubProductModelList.get(getAdapterPosition()).getEbay_sub_product_product_id());
                    b.putString("ebayId", (ebaySubProductModelList.get(getAdapterPosition()).getEbay_id()));
                    in.putExtras(b);
                    context.startActivity(in);
                }
            });
        }
    }

    private void addToCartMethod(final String productID) {
        ((BaseActivity) context).showProgressDialog("Please Wait..");
        StringRequest stringRequestNew = new StringRequest(com.android.volley.Request.Method.POST, Urls.ADD_TO_CART,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        Log.d("TAG", "response" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                Toast.makeText(context, "Please check your cart", Toast.LENGTH_LONG).show();
                                JSONObject jsonObjectItemsAddToCart = jsonObject.getJSONObject("response");
                                BaseActivity activity = (BaseActivity) context;
                                BaseActivity.finalCartCount = jsonObjectItemsAddToCart.getInt("count_product");
                                activity.refreshCartCounter();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("TAG", "ERROR_LOGIN: " + e.getMessage());
                        }
                        ((BaseActivity) context).dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                        ((BaseActivity) context).dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", UserData.USER_ID);
                params.put("product_id", productID);
                params.put("qty", "1");
                Log.d("TAG", "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);
    }
}




