package zwm.example.shubhangi.industrialservopart.fragment;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.adapter.NavigationDrawerAdapter;
import zwm.example.shubhangi.industrialservopart.model.NavDrawerItem;
import zwm.example.shubhangi.industrialservopart.util.CommonApi;
import zwm.example.shubhangi.industrialservopart.util.UserData;

import static zwm.example.shubhangi.industrialservopart.util.UserData.LOGGED_IN;

/**
 * Created by shubhangi on 12/5/2017.
 */

public class FragmentDrawer extends Fragment {
    private static String TAG = FragmentDrawer.class.getCanonicalName();

    @BindView(R.id.drawerList)
    RecyclerView recyclerView;
    @BindView(R.id.textUserName)
    TextView textUserName;



    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private View containerView;
    private NavigationDrawerAdapter adapter;
    private static String[] titles = null;
    private FragmentDrawerListener drawerListener;
    private CommonApi commonApi;


    public int[] menuImages = {R.drawable.ic_home, R.drawable.ic_cogs, R.drawable.ic_wrench,
            R.drawable.ic_user, R.drawable.ic_list_alt,R.drawable.ic_truck, R.drawable.ic_sign_in};
   /* R.drawable.ic_user_plus, R.drawable.ic_life_ring,*/
    public FragmentDrawer() {

    }

    public void setDrawerListener(FragmentDrawerListener listener) {
        this.drawerListener = listener;
    }

    public static List<NavDrawerItem> getData() {
        List<NavDrawerItem> data = new ArrayList<>();

        // preparing navigation drawer items
        for (int i = 0; i < titles.length; i++) {
            NavDrawerItem navItem = new NavDrawerItem();
            navItem.setTitle(titles[i]);
            data.add(navItem);
        }
        return data;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        commonApi = CommonApi.getInstance(getActivity());

        //titles = getActivity().getResources().getStringArray(R.array.nav_drawer_labels);
        if (LOGGED_IN) {
            titles = getActivity().getResources().getStringArray(R.array.nav_drawer_labels);
        } else {
            titles = getActivity().getResources().getStringArray(R.array.nav_drawer_labels_logout);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        ButterKnife.bind(this, layout);
        textUserName.setText(UserData.USER_FULL_NAME);


        //textUserName.setText("Welcome " + UserData.BILLING_FIRST_NAME);
        adapter = new NavigationDrawerAdapter(getActivity(), getData(), menuImages);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                drawerListener.onDrawerItemSelected(view, position);
                mDrawerLayout.closeDrawer(containerView);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        return layout;
    }

    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
               // getActivity().invalidateOptionsMenu();
                refreshDrawerItems();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
//                toolbar.setAlpha(1 - slideOffset / 2);
                mDrawerLayout.setScrimColor(Color.TRANSPARENT);
            }
        };

        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }
    public static interface ClickListener {
        public void onClick(View view, int position);

        public void onLongClick(View view, int position);
    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }

            });

        }


        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        }
    }
    public interface FragmentDrawerListener {
        public void onDrawerItemSelected(View view, int position);
    }

    public void refreshDrawerItems() {

        titles = LOGGED_IN ? getActivity().getResources().getStringArray(R.array.nav_drawer_labels) :
                getActivity().getResources().getStringArray(R.array.nav_drawer_labels_logout);

        adapter = new NavigationDrawerAdapter(getActivity(), getData(), menuImages);
        recyclerView.setAdapter(adapter);

        textUserName.setText(UserData.USER_FULL_NAME );
       /* if (LOGGED_IN && !(UserData.USER_ADDRESS_CITY_NAME.equals(""))) {
            user_location.setText(UserData.USER_ADDRESS_CITY_NAME + "," + commonApi.splitString(UserData.USER_ADDRESS_COUNTRY));
        }*/

    }
}



