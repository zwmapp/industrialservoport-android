package zwm.example.shubhangi.industrialservopart.model;

/**
 * Created by shubhangi on 4/26/2018.
 */

public class SearchResultModel {
    private String search_result_brand_part_id;
    private String search_result_brand_manufacturer_ID;
    private String search_result_brand_manufacturer_part_number;
    private String search_result_brand_manufacturer_description;

    public String getSearch_result_brand_part_id() {
        return search_result_brand_part_id;
    }

    public void setSearch_result_brand_part_id(String search_result_brand_part_id) {
        this.search_result_brand_part_id = search_result_brand_part_id;
    }

    public String getSearch_result_brand_manufacturer_ID() {
        return search_result_brand_manufacturer_ID;
    }

    public void setSearch_result_brand_manufacturer_ID(String search_result_brand_manufacturer_ID) {
        this.search_result_brand_manufacturer_ID = search_result_brand_manufacturer_ID;
    }

    public String getSearch_result_brand_manufacturer_part_number() {
        return search_result_brand_manufacturer_part_number;
    }

    public void setSearch_result_brand_manufacturer_part_number(String search_result_brand_manufacturer_part_number) {
        this.search_result_brand_manufacturer_part_number = search_result_brand_manufacturer_part_number;
    }

    public String getSearch_result_brand_manufacturer_description() {
        return search_result_brand_manufacturer_description;
    }

    public void setSearch_result_brand_manufacturer_description(String search_result_brand_manufacturer_description) {
        this.search_result_brand_manufacturer_description = search_result_brand_manufacturer_description;
    }
}
