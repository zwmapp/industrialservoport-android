package zwm.example.shubhangi.industrialservopart.activity;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.adapter.MyOrderDetailsAdapter;
import zwm.example.shubhangi.industrialservopart.model.MyOrderDetailsModel;
import zwm.example.shubhangi.industrialservopart.util.MyApplication;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.util.UserData;

/**
 * Created by shubhangi on 4/13/2018.
 */

public class MyOrderDetailsActivity extends BaseActivity {
    public static String TAG = MyOrderDetailsActivity.class.getCanonicalName();
    @BindView(R.id.tvToolbarText)
    TextView tvToolbarText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerViewMyOrderDetails)
    RecyclerView recyclerViewMyOrderDetails;

    String orderIdFromOrderListing;

    // Order
    List<MyOrderDetailsModel> myOrderDetailsModelList;
    MyOrderDetailsAdapter myOrderDetailsAdapter;
    // Order


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order_detail);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tvToolbarText.setText("My Order Details");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Bundle b = getIntent().getExtras();
        orderIdFromOrderListing = b.getString("orderId");
        Log.d("TAG", "orderIdFromOrderListing" + orderIdFromOrderListing);

        //order
        myOrderDetailsModelList = new ArrayList<>();
        recyclerViewMyOrderDetails.setHasFixedSize(true);
        //order

        getMyOrderDetails();
    }

    private void getMyOrderDetails() {
        showProgressDialog("Please Wait..");
        StringRequest stringRequestNew = new StringRequest(Request.Method.POST, Urls.SINGLE_ORDER_DETAILS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "responseS" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                JSONObject jsonObjectResponse = jsonObject.getJSONObject("response");
                                JSONArray jsonObjectCartItemsArray = jsonObjectResponse.getJSONArray("products");
                                Log.d(TAG, "jsonObjectCartItemsArray" + jsonObjectCartItemsArray);
                                for (int j = 0; j < jsonObjectCartItemsArray.length(); j++) {
                                    MyOrderDetailsModel myOrderDetailsModel = new MyOrderDetailsModel();
                                    try {
                                        JSONObject jsonObj = jsonObjectCartItemsArray.getJSONObject(j);
                                        myOrderDetailsModel.setOrdered_details_product_id(jsonObj.getString("ID"));
                                        myOrderDetailsModel.setOrdered_details_product_title(jsonObj.getString("title"));
                                        myOrderDetailsModel.setOrdered_details_product_image(jsonObj.getString("image"));
                                        myOrderDetailsModel.setOrdered_details_product_price(jsonObj.getString("price"));
                                        myOrderDetailsModel.setOrdered_details_product_quantity(jsonObj.getString("quantity"));

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    myOrderDetailsModelList.add(myOrderDetailsModel);
                                }
                                myOrderDetailsAdapter = new MyOrderDetailsAdapter(MyOrderDetailsActivity.this, myOrderDetailsModelList);
                                recyclerViewMyOrderDetails.setAdapter(myOrderDetailsAdapter);

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "ERROR_LOGIN: " + e.getMessage());
                        }
                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MyOrderDetailsActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", UserData.USER_ID);
                params.put("order_id", orderIdFromOrderListing);
                Log.d(TAG, "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);
    }

}
