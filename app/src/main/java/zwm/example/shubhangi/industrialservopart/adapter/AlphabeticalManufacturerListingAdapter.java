package zwm.example.shubhangi.industrialservopart.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.activity.BrandManufacturerActivity;
import zwm.example.shubhangi.industrialservopart.model.AlphabeticalManufacturerListingModel;

/**
 * Created by shubhangi on 1/20/2018.
 */

public class AlphabeticalManufacturerListingAdapter extends RecyclerView.Adapter<AlphabeticalManufacturerListingAdapter.ViewHolder> {

    List<AlphabeticalManufacturerListingModel> alphabeticalManufacturerListingModelList;
    AlphabeticalManufacturerListingModel alphabeticalManufacturerListingModel;
    private Context context;

    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public AlphabeticalManufacturerListingAdapter(Context context, List<AlphabeticalManufacturerListingModel> alphabeticalManufacturerListingModelList) {
        super();
        this.context = context;
        this.alphabeticalManufacturerListingModelList = alphabeticalManufacturerListingModelList;
    }

    // inflates the cell layout from xml when needed
    @Override
    public AlphabeticalManufacturerListingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.alphabetical_manufacturer_listing_recyclerview_items, parent, false);
        return new AlphabeticalManufacturerListingAdapter.ViewHolder(view);
    }

    // binds the data to the textView in each cell
    @Override
    public void onBindViewHolder(AlphabeticalManufacturerListingAdapter.ViewHolder holder, int position) {
        alphabeticalManufacturerListingModel = alphabeticalManufacturerListingModelList.get(position);

        // holder.alphabeticalManufacturerListingId.setText(alphabeticalManufacturerListingModel.getAlphabetical_manufacturer_listing_id());
        holder.alphabeticalManufacturerListingTitle.setText(alphabeticalManufacturerListingModel.getAlphabetical_manufacturer_listing_title());
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return alphabeticalManufacturerListingModelList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView alphabeticalManufacturerListingId, alphabeticalManufacturerListingTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            // alphabeticalManufacturerListingId = itemView.findViewById(R.id.alphabeticalManufacturerListingId);
            alphabeticalManufacturerListingTitle = itemView.findViewById(R.id.alphabeticalManufacturerListingTitle);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            String moreProductTemp = alphabeticalManufacturerListingModelList.get(getAdapterPosition()).getAlphabetical_manufacturer_listing_id();
            //Toast.makeText(context, "moreProductTemp" + moreProductTemp, Toast.LENGTH_SHORT).show();
            Intent in = new Intent(context,BrandManufacturerActivity.class);
            in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle b = new Bundle();
            b.putString("manIDFromCT", alphabeticalManufacturerListingModelList.get(getAdapterPosition()).getAlphabetical_manufacturer_listing_id());
            in.putExtras(b);
            context.startActivity(in);
        }
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}






