package zwm.example.shubhangi.industrialservopart.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.util.CommonApi;
import zwm.example.shubhangi.industrialservopart.util.Constant;
import zwm.example.shubhangi.industrialservopart.util.MyApplication;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.util.UserData;

/**
 * Created by shubhangi on 12/7/2017.
 */

public class SignUpActivity extends BaseActivity {

    @BindView(R.id.etFullName)
    EditText etFullName;
    @BindView(R.id.etEmailAddress)
    EditText etEmailAddress;
    @BindView(R.id.etPhone)
    EditText etPhone;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.etUserName)
    EditText etUserName;
    @BindView(R.id.etAddress)
    EditText etAddress;
    @BindView(R.id.signUpLayout)
    LinearLayout signUpLayout;

    private String errorMsg;
    private SharedPreferences.Editor editor;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        errorMsg = "";
    }

    @OnClick(R.id.signUpButton)
    public void onViewClicked() {
        commonApi.errorMsg = "";
        errorMsg = commonApi.checkRequiredFields(signUpLayout);
        if (errorMsg.equals("")) {
            uploadNewAccountData();
        } else {
            commonApi.showSnackBar(signUpLayout, "" + errorMsg, 1);
            return;
        }
    }

    private void uploadNewAccountData() {
        if (!commonApi.checkEmailValidation(etEmailAddress.getText().toString().trim())) {
            commonApi.showSnackBar(signUpLayout, Constant.EMAIL_VALIDATION + errorMsg, 1);
            return;
        }
        if (etPassword.getText().length() < 6) {
            commonApi.showSnackBar(signUpLayout, Constant.PASSWORD_LENGTH_CHECKING + errorMsg, 1);
            return;
        }
        showProgressDialog("Please wait..");
        StringRequest stringRequestNew = new StringRequest(Request.Method.POST, Urls.SIGN_UP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "responseS" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                JSONObject jsonObjectItemsSignUp = jsonObject.getJSONObject("response");
                                UserData.USER_ID = jsonObjectItemsSignUp.getString("user_ID");
                                UserData.USER_FULL_NAME = jsonObjectItemsSignUp.getString("full_name");
                                UserData.USER_EMAIL = jsonObjectItemsSignUp.getString("email");
                                UserData.LOGGED_IN = true;


                                editor = getSharedPreferences("UserPref", 0).edit();
                                editor.putString("key_userid", UserData.USER_ID);
                                editor.putString("key_userfullname", UserData.USER_FULL_NAME);
                                editor.putString("key_email", UserData.USER_EMAIL);
                                editor.putBoolean("key_loggedIn", UserData.LOGGED_IN);

                                editor.apply();
                                SignUpActivity.this.finish();
                                commonApi.openNewScreen(MainActivity.class, null);
                            }
                            else
                            {
                                String loginError = jsonObject.getJSONObject("response").getString("msg_for_user");
                                Toast.makeText(SignUpActivity.this, loginError, Toast.LENGTH_SHORT).show();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "ERROR_LOGIN: " + e.getMessage());
                        }
                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SignUpActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", etFullName.getText().toString().trim());
                params.put("email", etEmailAddress.getText().toString().trim());
                params.put("username", etUserName.getText().toString().trim());
                params.put("phone", etPhone.getText().toString().trim());
                params.put("password", etPassword.getText().toString().trim());
                params.put("address",etAddress.getText().toString().trim());
                params.put("contactUs10","1");
                Log.d(TAG, "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);
    }
}
