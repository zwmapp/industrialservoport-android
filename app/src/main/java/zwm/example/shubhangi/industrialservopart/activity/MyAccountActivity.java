package zwm.example.shubhangi.industrialservopart.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.util.Constant;
import zwm.example.shubhangi.industrialservopart.util.MyApplication;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.util.UserData;

/**
 * Created by shubhangi on 1/4/2018.
 */

public class MyAccountActivity extends BaseActivity {
    @BindView(R.id.tvToolbarText)
    TextView tvToolbarText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.etEmailAddress)
    EditText etEmailAddress;
    @BindView(R.id.etUserName)
    EditText etUserName;
    @BindView(R.id.etFullName)
    EditText etFullName;
    @BindView(R.id.etPhone)
    EditText etPhone;
    @BindView(R.id.etAddressLine1)
    EditText etAddressLine1;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.etPasswordTextInput)
    TextInputLayout etPasswordTextInput;
    @BindView(R.id.myAccountLayout)
    LinearLayout myAccountLayout;
    @BindView(R.id.buttonSaveData)
    Button buttonSaveData;
    String errorMsg;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myaccount);
        ButterKnife.bind(this);
        errorMsg = "";


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tvToolbarText.setText("My Account");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        etEmailAddress.setEnabled(false);
        buttonSaveData.setEnabled(false);
        etPasswordTextInput.setVisibility(View.GONE);
        etFullName.setEnabled(false);
        etUserName.setEnabled(false);
        etPhone.setEnabled(false);
        etAddressLine1.setEnabled(false);

        getUserDetail();
    }

    private void getUserDetail() {
        showProgressDialog("Please Wait..");
        StringRequest stringRequestNew = new StringRequest(Request.Method.POST, Urls.USER_DETAIL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "response" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                JSONObject jsonObjectItems = jsonObject.getJSONObject("response");

                                UserData.USER_FULL_NAME = jsonObjectItems.getString("full_name");
                                UserData.USER_PHONE = jsonObjectItems.getString("phone_number");
                                UserData.USER_USERNAME = jsonObjectItems.getString("user_name");
                                UserData.USER_ADDRESS = jsonObjectItems.getString("address");
                                UserData.USER_EMAIL = jsonObjectItems.getString("email");

                                editor = getSharedPreferences("UserPref", 0).edit();
                                editor.putString("key_userid", UserData.USER_ID);
                                editor.putString("key_userUsername", UserData.USER_USERNAME );
                                editor.putString("key_userfullname",UserData.USER_FULL_NAME);
                                editor.putString("key_email", UserData.USER_EMAIL);
                                editor.putString("key_phone", UserData.USER_PHONE);
                                editor.putString("key_address", UserData.USER_ADDRESS);
                                editor.apply();


                                etFullName.setText(UserData.USER_FULL_NAME);
                                etEmailAddress.setText(UserData.USER_EMAIL);
                                etUserName.setText(UserData.USER_USERNAME);
                                etPhone.setText(UserData.USER_PHONE);
                                etAddressLine1.setText(UserData.USER_ADDRESS);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "ERROR_LOGIN: " + e.getMessage());
                        }
                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MyAccountActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_ID", UserData.USER_ID);
                Log.d(TAG, "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);
    }


    @OnClick({R.id.editMyAccountDetail, R.id.buttonSaveData})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.editMyAccountDetail:
                etPasswordTextInput.setVisibility(View.VISIBLE);
                editTextMethod();
                break;
            case R.id.buttonSaveData:
                commonApi.errorMsg = "";
                errorMsg = commonApi.checkRequiredFields(myAccountLayout);
                if (errorMsg.equals("")) {
                    myAccountDetails();
                } else {
                    commonApi.showSnackBar(myAccountLayout, "" + errorMsg, 1);
                    return;
                }
                break;
        }
    }


    private void editTextMethod() {
        buttonSaveData.setEnabled(true);
        etEmailAddress.setEnabled(false);
        etFullName.setEnabled(true);
        etUserName.setEnabled(true);
        etPhone.setEnabled(true);
        etAddressLine1.setEnabled(true);

    }

    private void myAccountDetails() {
        if (!commonApi.checkEmailValidation(etEmailAddress.getText().toString().trim())) {
            commonApi.showSnackBar(myAccountLayout, "Please enter a valid email address" + errorMsg, 1);
            return;
        }
        /*if (!etPassword.equals("") &&etPassword.getText().length() < 6) {
            commonApi.showSnackBar(myAccountLayout, Constant.PASSWORD_LENGTH_CHECKING + errorMsg, 1);
            return;
        }*/

        showProgressDialog("Please Wait..");
        StringRequest stringRequestNew = new StringRequest(Request.Method.POST, Urls.UPDATE_USER_DETAIL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "response" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                Toast.makeText(MyAccountActivity.this, "Updated Successfully", Toast.LENGTH_SHORT).show();
                            } else {
                                String loginError = jsonObject.getJSONObject("response").getString("msg_for_user");
                                Toast.makeText(MyAccountActivity.this, loginError, Toast.LENGTH_SHORT).show();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "ERROR_LOGIN: " + e.getMessage());
                        }
                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MyAccountActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_ID", UserData.USER_ID);
                params.put("name", etFullName.getText().toString().trim());
                params.put("phone", etPhone.getText().toString().trim());
                params.put("email", etEmailAddress.getText().toString().trim());
                params.put("password", etPassword.getText().toString().trim());
                params.put("username", etUserName.getText().toString().trim());
                params.put("address", etAddressLine1.getText().toString().trim());
                params.put("contactUs10", "1");
                Log.d(TAG, "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);
    }

}


