package zwm.example.shubhangi.industrialservopart.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.activity.BaseActivity;
import zwm.example.shubhangi.industrialservopart.model.MyOrderDetailsModel;
import zwm.example.shubhangi.industrialservopart.util.MyApplication;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.util.UserData;

/**
 * Created by shubhangi on 4/13/2018.
 */

public class MyOrderDetailsAdapter extends RecyclerView.Adapter<MyOrderDetailsAdapter.ViewHolder> {

    List<MyOrderDetailsModel> myOrderDetailsModelList;
    MyOrderDetailsModel myOrderDetailsModel;
    private Context context;


    // data is passed into the constructor
    public MyOrderDetailsAdapter(Context context, List<MyOrderDetailsModel> myOrderDetailsModelList) {
        super();
        this.context = context;
        this.myOrderDetailsModelList = myOrderDetailsModelList;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_my_order_details_items, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        myOrderDetailsModel = myOrderDetailsModelList.get(position);
        holder.productNameInOrderDetails.setText(myOrderDetailsModel.getOrdered_details_product_title());
        holder.productPriceInOrderDetails.setText("$" + myOrderDetailsModel.getOrdered_details_product_price());
        holder.productQuantityInOrderDetails.setText("Quantity - " + myOrderDetailsModel.getOrdered_details_product_quantity());
        Glide.with(context)
                .load(myOrderDetailsModel.getOrdered_details_product_image())
                .into(holder.imageViewProductInOrderDetails);
    }


    // total number of cells
    @Override
    public int getItemCount() {
        return myOrderDetailsModelList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageViewProductInOrderDetails;

        public TextView productNameInOrderDetails, productPriceInOrderDetails, productQuantityInOrderDetails;


        public ViewHolder(final View itemView) {
            super(itemView);

            imageViewProductInOrderDetails = itemView.findViewById(R.id.imageViewProductInOrderDetails);
            productNameInOrderDetails = itemView.findViewById(R.id.productNameInOrderDetails);
            productPriceInOrderDetails = itemView.findViewById(R.id.productPriceInOrderDetails);
            productQuantityInOrderDetails = itemView.findViewById(R.id.productQuantityInOrderDetails);


        }
    }

}






