package zwm.example.shubhangi.industrialservopart.model;

/**
 * Created by shubhangi on 1/8/2018.
 */

public class BrandListingModel {
    private String barnd_id;
    private String brand_title;
    private String brand_image;
    private String brand_description;

    public String getBarnd_id() {
        return barnd_id;
    }

    public void setBarnd_id(String barnd_id) {
        this.barnd_id = barnd_id;
    }

    public String getBrand_title() {
        return brand_title;
    }

    public void setBrand_title(String brand_title) {
        this.brand_title = brand_title;
    }

    public String getBrand_image() {
        return brand_image;
    }

    public void setBrand_image(String brand_image) {
        this.brand_image = brand_image;
    }

    public String getBrand_description() {
        return brand_description;
    }

    public void setBrand_description(String brand_description) {
        this.brand_description = brand_description;
    }


}
