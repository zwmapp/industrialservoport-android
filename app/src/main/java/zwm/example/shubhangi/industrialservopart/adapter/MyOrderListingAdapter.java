package zwm.example.shubhangi.industrialservopart.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.activity.BaseActivity;
import zwm.example.shubhangi.industrialservopart.activity.MyOrderActivity;
import zwm.example.shubhangi.industrialservopart.activity.MyOrderDetailsActivity;
import zwm.example.shubhangi.industrialservopart.model.CartItemModel;
import zwm.example.shubhangi.industrialservopart.model.MyOrderListModel;
import zwm.example.shubhangi.industrialservopart.util.MyApplication;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.util.UserData;

/**
 * Created by shubhangi on 4/12/2018.
 */

public class MyOrderListingAdapter extends RecyclerView.Adapter<MyOrderListingAdapter.ViewHolder> {

    List<MyOrderListModel> myOrderListModelList;
    MyOrderListModel myOrderListModel;
    private Context context;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public MyOrderListingAdapter(Context context, List<MyOrderListModel> myOrderListModelList) {
        super();
        this.context = context;
        this.myOrderListModelList = myOrderListModelList;
    }

    // inflates the cell layout from xml when needed
    @Override
    public MyOrderListingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_my_order_list_items, parent, false);
        return new MyOrderListingAdapter.ViewHolder(view);
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(MyOrderListingAdapter.ViewHolder holder, int position) {
        myOrderListModel = myOrderListModelList.get(position);
        holder.orderId.setText("Order Id -  "+  myOrderListModel.getOrder_id());
        holder.orderId.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        holder.orderDate.setText("Date \n"+  myOrderListModel.getOrder_date());
        holder.orderPaymentStatus.setText("Payment Status -  " + myOrderListModel.getOrder_payment_status());
        holder.orderPaymentTransactionId.setText("Transaction Id -  " +  myOrderListModel.getOrder_payment_txn_id());
        holder.orderPaymentType.setText("Payment Type -  " +  myOrderListModel.getOrder_payment_type());
        holder.totalBillOrder.setText("Total Bill -  " + "$" +  myOrderListModel.getOrder_total_bill());
        holder.totalOrderQty.setText("Quantity -  " + myOrderListModel.getOrdered_product_quantity());

        holder.orderIdToSend = myOrderListModelList.get(position).getOrder_id();
    }


    // total number of cells
    @Override
    public int getItemCount() {
        return myOrderListModelList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView orderId, orderDate, orderPaymentStatus, orderPaymentTransactionId,orderPaymentType,
                totalBillOrder,totalOrderQty;
        String orderIdToSend;

        public ViewHolder(final View itemView) {
            super(itemView);

            orderId = itemView.findViewById(R.id.orderId);
            orderDate = itemView.findViewById(R.id.orderDate);
            orderPaymentStatus = itemView.findViewById(R.id.orderPaymentStatus);
            orderPaymentTransactionId = itemView.findViewById(R.id.orderPaymentTransactionId);
            orderPaymentType = itemView.findViewById(R.id.orderPaymentType);
            totalBillOrder = itemView.findViewById(R.id.totalBillOrder);
            totalOrderQty = itemView.findViewById(R.id.totalOrderQty);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            Intent in = new Intent(context, MyOrderDetailsActivity.class);
            Bundle b = new Bundle();
            b.putString("orderId", orderIdToSend);
            in.putExtras(b);
            context.startActivity(in);
        }
    }
    // allows clicks events to be caught
    public void setClickListener(MyOrderListingAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

}





