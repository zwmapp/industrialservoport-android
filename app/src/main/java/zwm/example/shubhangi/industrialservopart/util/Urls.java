package zwm.example.shubhangi.industrialservopart.util;

import butterknife.BindView;

/**
 * Created by shubhangi on 1/2/2018.
 */

public class Urls {

    public static final String BASE_URL = "https://www.repairservomotors.com/webservice/api.php?action=";
    public static final String BASE_URL_FOR_IMAGE = "https://www.repairservomotors.com/";
    public static final String BASE_URL_FOR_BRAND_IMAGE = "https://www.repairservomotors.com/zoom-admin/";

    public static final String SIGN_UP = BASE_URL + "sign_up";
    public static final String LOGIN = BASE_URL + "sign_in";
    public static final String FORGOT_PASSWORD = BASE_URL + "forgot_pass";

    public static final String USER_DETAIL = BASE_URL + "view_user";
    public static final String UPDATE_USER_DETAIL = BASE_URL + "update_user";

    public static final String HOME_PAGE = BASE_URL + "home_page";
    public static final String EBAY_CATEGORIES = BASE_URL + "cat_cat";
    public static final String EBAY_CATEGORIES_SUB_PRODUCT = BASE_URL + "cat_prod";
    public static final String EBAY_CATEGORIES_SUB_PRODUCT_DETAIL_DESCRIPTION = BASE_URL + "detail_item";

    public static final String ALL_BRAND = BASE_URL + "man_list";
    public static final String ALL_BRAND_MANUFACTURER = BASE_URL + "parts_list";
    public static final String MANUFACTURER_DETAIL = BASE_URL + "manufac_details";

    public static final String ADD_TO_CART = BASE_URL + "add_cart";
    public static final String GET_CART = BASE_URL + "view_cart";
    public static final String REMOVE_FROM_CART = BASE_URL + "del_cart";
    public static final String UPDATE_CART = BASE_URL + "update_cart";

    public static final String ALPHABETICAL_MANUFACTURER_LISTING = BASE_URL + "man_rp";
    public static final String ALPHABETICAL_BRAND_MANUFACTURER_LISTINGS = BASE_URL + "man_part";
    public static final String ALPHABETICAL_BRAND_MANUFACTURER_LISTINGS_DEATIL = BASE_URL + "part_num";
    public static final String MANUFACTURER_REQUEST_QUOTE = BASE_URL + "man_request_quote";

    public static final String PLACE_ORDER = BASE_URL + "check_out";
    public static final String ALL_ORDER_LIST = BASE_URL + "all_order";
    public static final String SINGLE_ORDER_DETAILS = BASE_URL + "single_order";

    public static final String MAKE_AN_OFFER = BASE_URL + "make_offer";

    public static final String SEARCH_PRODUCT_BY_PART_NUMBER = BASE_URL + "search_ivntr";
}
