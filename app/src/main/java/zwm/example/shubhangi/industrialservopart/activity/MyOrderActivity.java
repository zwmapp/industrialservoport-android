package zwm.example.shubhangi.industrialservopart.activity;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.adapter.MyOrderListingAdapter;
import zwm.example.shubhangi.industrialservopart.model.MyOrderListModel;
import zwm.example.shubhangi.industrialservopart.util.MyApplication;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.util.UserData;

/**
 * Created by shubhangi on 12/7/2017.
 */

public class MyOrderActivity extends BaseActivity {
    @BindView(R.id.tvToolbarText)
    TextView tvToolbarText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerViewMyOrder)
    RecyclerView recyclerViewMyOrder;
    @BindView(R.id.tv_alert)
    TextView tv_alert;

    // Cart
    List<MyOrderListModel> myOrderListModelList;
    MyOrderListingAdapter myOrderListingAdapter;
    // Cart

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        ButterKnife.bind(this);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tvToolbarText.setText("My Order");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        //order
        myOrderListModelList = new ArrayList<>();
        recyclerViewMyOrder.setHasFixedSize(true);
        //order

        getMyOrderList();
    }

    private void getMyOrderList() {
        showProgressDialog("Please Wait..");
        StringRequest stringRequestNew = new StringRequest(Request.Method.POST, Urls.ALL_ORDER_LIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "responseS" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                JSONArray jsonArrayResponse = jsonObject.getJSONArray("response");
                                Log.d(TAG, "jsonArrayResponse" + jsonArrayResponse);

                                for (int j = 0; j < jsonArrayResponse.length(); j++) {
                                    MyOrderListModel myOrderListModel = new MyOrderListModel();
                                    try {
                                        JSONObject jsonObj = jsonArrayResponse.getJSONObject(j);
                                        myOrderListModel.setOrder_id(jsonObj.getString("order_id"));
                                        myOrderListModel.setOrder_date(jsonObj.getString("date"));
                                        myOrderListModel.setOrdered_product_quantity(jsonObj.getString("no_total_items"));
                                        myOrderListModel.setOrder_total_bill(jsonObj.getString("total"));
                                        myOrderListModel.setOrder_payment_txn_id(jsonObj.getString("payment_txn_id"));
                                        myOrderListModel.setOrder_payment_status(jsonObj.getString("payment_status"));
                                        myOrderListModel.setOrder_payment_type(jsonObj.getString("payment_type"));

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    myOrderListModelList.add(myOrderListModel);
                                }
                                if (myOrderListModelList.size() > 0) {
                                    recyclerViewMyOrder.setVisibility(View.VISIBLE);
                                    tv_alert.setVisibility(View.GONE);
                                } else {
                                    recyclerViewMyOrder.setVisibility(View.GONE);
                                    tv_alert.setVisibility(View.VISIBLE);
                                }
                                myOrderListingAdapter = new MyOrderListingAdapter(MyOrderActivity.this, myOrderListModelList);
                                recyclerViewMyOrder.setAdapter(myOrderListingAdapter);

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "ERROR_LOGIN: " + e.getMessage());
                        }
                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MyOrderActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", UserData.USER_ID);
                Log.d(TAG, "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);
    }
}
