package zwm.example.shubhangi.industrialservopart.activity;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.adapter.EbaySubProductAdapter;
import zwm.example.shubhangi.industrialservopart.model.EbaySubProductModel;
import zwm.example.shubhangi.industrialservopart.util.MyApplication;
import zwm.example.shubhangi.industrialservopart.util.Urls;

/**
 * Created by shubhangi on 1/5/2018.
 */

public class EbaySubProductDetailActivity extends BaseActivity {
    @BindView(R.id.tvToolbarText)
    TextView tvToolbarText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerViewEBaySubProduct)
    RecyclerView recyclerViewEBaySubProduct;
    @BindView(R.id.ebayProductItemPositionTitle)
    TextView ebayProductItemPositionTitle;
    List<EbaySubProductModel> ebaySubProductModelList;
    int numberOfColumns = 2;
    EbaySubProductAdapter ebaySubProductAdapter;
    int ebayProductItemPosition;
    int pageNo = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ebay_sub_product_detail);
        ButterKnife.bind(this);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tvToolbarText.setText("Ebay Product Detail");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Bundle b = getIntent().getExtras();
        ebayProductItemPosition = b.getInt("ebayProductItemPosition");
        Log.d(TAG, "ebayProductItemPosition" + ebayProductItemPosition);

        Bundle b1 = getIntent().getExtras();
        String d = b1.getString("ebayProductItemPositionTitle");
        ebayProductItemPositionTitle.setText(d);

        //Product
        ebaySubProductModelList = new ArrayList<>();
        //recyclerViewEBaySubProduct.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        recyclerViewEBaySubProduct.setHasFixedSize(true);
        //Product

        eBaySubProductDetail();
    }

    private void eBaySubProductDetail() {
        showProgressDialog("Please wait..");
        StringRequest stringRequestNew = new StringRequest(Request.Method.POST, Urls.EBAY_CATEGORIES_SUB_PRODUCT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "responseS" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                JSONArray jsonArrayItems = jsonObject.getJSONArray("response");
                                for (int i = 0; i < jsonArrayItems.length(); i++) {
                                    EbaySubProductModel ebaySubProductModel = new EbaySubProductModel();
                                    try {
                                        JSONObject rec = jsonArrayItems.getJSONObject(i);
                                        ebaySubProductModel.setEbay_sub_product_product_title(rec.getString("title"));
                                        ebaySubProductModel.setEbay_sub_product_product_price(rec.getString("price"));
                                        ebaySubProductModel.setEbay_sub_product_product_image(rec.getString("image"));
                                        ebaySubProductModel.setEbay_id(rec.getString("ebay_id"));
                                        ebaySubProductModel.setEbay_sub_product_product_id(rec.getString("ID"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    dismissProgressDialog();
                                    ebaySubProductModelList.add(ebaySubProductModel);

                                }
                                ebaySubProductAdapter = new EbaySubProductAdapter(EbaySubProductDetailActivity.this, ebaySubProductModelList);
                                recyclerViewEBaySubProduct.setAdapter(ebaySubProductAdapter);

                            } else {
                                //String loginError = jsonObject.getJSONObject("response").getString("msg_for_user");
                                Toast.makeText(EbaySubProductDetailActivity.this, "No more product to show", Toast.LENGTH_SHORT).show();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "ERROR_LOGIN: " + e.getMessage());
                        }
                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "error" + error);
                        Toast.makeText(EbaySubProductDetailActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("ecat_id", String.valueOf(ebayProductItemPosition));
                params.put("page_no", String.valueOf(pageNo));
                Log.d(TAG, "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);
    }

    @OnClick(R.id.viewNextTextView)
    public void onViewClicked() {
        pageNo++;
        eBaySubProductDetail();
    }
}

