package zwm.example.shubhangi.industrialservopart.activity;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.adapter.SearchResultAdapter;
import zwm.example.shubhangi.industrialservopart.model.SearchResultModel;
import zwm.example.shubhangi.industrialservopart.util.MyApplication;
import zwm.example.shubhangi.industrialservopart.util.Urls;

/**
 * Created by shubhangi on 4/26/2018.
 */

public class SearchResultActivity extends BaseActivity {
    @BindView(R.id.tvToolbarText)
    TextView tvToolbarText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerViewManufacturerSearchResult)
    RecyclerView recyclerViewManufacturerSearchResult;

    List<SearchResultModel> searchResultModelList;
    int numberOfColumns = 2;
    SearchResultAdapter searchResultAdapter;

    String searchQuery;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tvToolbarText.setText("Search  Result");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        Bundle b = getIntent().getExtras();
        searchQuery = b.getString("part_number_for_search");
        Log.d(TAG, "searchQuery" + searchQuery);

        //Brand Manufacturer
        searchResultModelList = new ArrayList<>();
        recyclerViewManufacturerSearchResult.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        recyclerViewManufacturerSearchResult.setHasFixedSize(true);
        //Brand Manufacturer


        getManufacturerDetail();
    }

    private void getManufacturerDetail() {
        showProgressDialog("Searching..");
        StringRequest stringRequestNew = new StringRequest(Request.Method.POST, Urls.SEARCH_PRODUCT_BY_PART_NUMBER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "responseS" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                searchResultModelList = new ArrayList<>();
                                JSONArray jsonArrayItems = jsonObject.getJSONArray("response");
                                for (int i = 0; i < jsonArrayItems.length(); i++) {
                                    SearchResultModel searchResultModel = new SearchResultModel();
                                    try {
                                        JSONObject rec = jsonArrayItems.getJSONObject(i);
                                        searchResultModel.setSearch_result_brand_part_id(rec.getString("ID"));
                                        searchResultModel.setSearch_result_brand_manufacturer_part_number(rec.getString("part_number"));
                                        searchResultModel.setSearch_result_brand_manufacturer_ID(rec.getString("manufacturer_id"));

                                        //searchResultModel.setSearch_result_brand_manufacturer_description(rec.getString("description"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    dismissProgressDialog();
                                    searchResultModelList.add(searchResultModel);

                                }
                                searchResultAdapter = new SearchResultAdapter(SearchResultActivity.this, searchResultModelList);
                                recyclerViewManufacturerSearchResult.setAdapter(searchResultAdapter);

                            } else {
                                String loginError = jsonObject.getJSONObject("response").getString("msg_for_user");
                                Toast.makeText(SearchResultActivity.this, loginError, Toast.LENGTH_SHORT).show();
                            }
                            /*if (searchResultModelList.size() < 0){
                                Toast.makeText(SearchResultActivity.this, "Result Found", Toast.LENGTH_SHORT).show();
                            }*/
                            /*else
                            {
                                Toast.makeText(SearchResultActivity.this, "Result Not Found", Toast.LENGTH_SHORT).show();

                            }*/
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "ERROR_LOGIN: " + e.getMessage());
                        }
                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "error" + error);
                        Toast.makeText(SearchResultActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("query", searchQuery);
                Log.d(TAG, "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);

    }
}
