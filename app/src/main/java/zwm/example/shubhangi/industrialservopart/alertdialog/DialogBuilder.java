package zwm.example.shubhangi.industrialservopart.alertdialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;



public class DialogBuilder {
    public Context context;
    private String title, msg;
    private onOkButtonListener listener;

    public DialogBuilder(Context context, String title, String msg, onOkButtonListener listener) {
        this.context = context;
        this.title = title;
        this.msg = msg;
        this.listener = listener;
    }

    public void showAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setIcon(null)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                listener.onClickOkButton(dialog);
                            }
                        }
                ).create();

        alertDialog.show();
    }

    public interface onOkButtonListener {
        void onClickOkButton(DialogInterface dialog);
    }
}
