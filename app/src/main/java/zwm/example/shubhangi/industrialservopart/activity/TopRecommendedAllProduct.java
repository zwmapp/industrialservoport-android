package zwm.example.shubhangi.industrialservopart.activity;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.adapter.TopRecommendedProductAdapter;
import zwm.example.shubhangi.industrialservopart.model.TopRecommendedProductListModel;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.volleyparser.GetServiceCall;

/**
 * Created by shubhangi on 1/5/2018.
 */

public class TopRecommendedAllProduct extends BaseActivity {
    @BindView(R.id.recyclerViewTopRecommendedAllProduct)
    RecyclerView recyclerViewTopRecommendedAllProduct;
    @BindView(R.id.tvToolbarText)
    TextView tvToolbarText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    List<TopRecommendedProductListModel> topRecommendedProductListModelList;
    int numberOfColumns = 2;
    TopRecommendedProductAdapter topRecommendedProductAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toprecommended_all_product);
        ButterKnife.bind(this);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tvToolbarText.setText("Top Recommended");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        topRecommendedProductListModelList = new ArrayList<>();
        recyclerViewTopRecommendedAllProduct.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        recyclerViewTopRecommendedAllProduct.setHasFixedSize(true);
        topRecommendedProductDetail();

    }

    private void topRecommendedProductDetail() {
        showProgressDialog("Loading..");
        new GetServiceCall(Urls.HOME_PAGE, GetServiceCall.TYPE_JSONOBJECT) {
            @Override
            public void response(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonO = jsonObject.optJSONObject("response");
                    JSONArray jsonArrayproducts = jsonO.getJSONArray("top_recomended");

                    for (int i = 0; i < jsonArrayproducts.length(); i++) {
                        TopRecommendedProductListModel topRecommendedProductListModel = new TopRecommendedProductListModel();
                        try {
                            JSONObject rec = jsonArrayproducts.getJSONObject(i);
                            topRecommendedProductListModel.setID(rec.getString("ID"));
                            topRecommendedProductListModel.setEbay_id(rec.getString("ebay_id"));
                            topRecommendedProductListModel.setTitle(rec.getString("title"));
                            topRecommendedProductListModel.setPrice(rec.getString("price"));
                            topRecommendedProductListModel.setImage(rec.getString("image"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dismissProgressDialog();
                        topRecommendedProductListModelList.add(topRecommendedProductListModel);
                    }
                    topRecommendedProductAdapter = new TopRecommendedProductAdapter(TopRecommendedAllProduct.this, topRecommendedProductListModelList);
                    recyclerViewTopRecommendedAllProduct.setAdapter(topRecommendedProductAdapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                // dismissProgressDialog();
            }

            @Override
            public void error(VolleyError error, String errorMsg) {
                Log.d(TAG, "error: " + error.toString());
                Log.d(TAG, "errorMsg: " + errorMsg.toString());
                dismissProgressDialog();
            }
        }.call();
    }
}
