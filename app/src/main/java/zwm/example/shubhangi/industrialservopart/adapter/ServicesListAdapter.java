package zwm.example.shubhangi.industrialservopart.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.net.URL;
import java.util.List;

import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.model.ServicesModel;
import zwm.example.shubhangi.industrialservopart.model.TopRecommendedProductListModel;
import zwm.example.shubhangi.industrialservopart.util.Urls;

/**
 * Created by shubhangi on 1/4/2018.
 */

public class ServicesListAdapter extends RecyclerView.Adapter<ServicesListAdapter.ViewHolder> {

    List<ServicesModel> servicesModelList;
    ServicesModel servicesModel;
    private Context context;

    // private ItemClickListener mClickListener;

    // data is passed into the constructor
    public ServicesListAdapter(Context context, List<ServicesModel> servicesModelList) {
        super();
        this.context = context;
        this.servicesModelList = servicesModelList;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ServicesListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.services_recyclerview_items, parent, false);
        return new ServicesListAdapter.ViewHolder(view);
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(ServicesListAdapter.ViewHolder holder, int position) {
        servicesModel = servicesModelList.get(position);

        holder.nameProduct.setText(servicesModel.getTitle());
        holder.descriptionProduct.setText(servicesModel.getDescription());
        Glide.with(context)
                .load(Urls.BASE_URL_FOR_IMAGE + servicesModel.getImage())
                .into(holder.imageProduct);
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return servicesModelList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameProduct,descriptionProduct;
        public ImageView imageProduct;

        public ViewHolder(View itemView) {
            super(itemView);
            nameProduct = itemView.findViewById(R.id.nameProduct);
            imageProduct =  itemView.findViewById(R.id.imageProduct);
            descriptionProduct = itemView.findViewById(R.id.descriptionProduct);

        }
    }


}



