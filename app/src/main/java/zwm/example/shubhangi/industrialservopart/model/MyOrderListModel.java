package zwm.example.shubhangi.industrialservopart.model;

/**
 * Created by shubhangi on 4/12/2018.
 */

public class MyOrderListModel {
    private String order_id;
    private String order_date;
    private String order_total_bill;
    private String order_payment_type;
    private String order_payment_status;
    private String order_payment_txn_id;

    private String ordered_product_id;
    private String ordered_product_title;
    private String ordered_product_image;
    private String ordered_product_price;
    private String ordered_product_quantity;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getOrder_total_bill() {
        return order_total_bill;
    }

    public void setOrder_total_bill(String order_total_bill) {
        this.order_total_bill = order_total_bill;
    }

    public String getOrder_payment_type() {
        return order_payment_type;
    }

    public void setOrder_payment_type(String order_payment_type) {
        this.order_payment_type = order_payment_type;
    }

    public String getOrder_payment_status() {
        return order_payment_status;
    }

    public void setOrder_payment_status(String order_payment_status) {
        this.order_payment_status = order_payment_status;
    }

    public String getOrder_payment_txn_id() {
        return order_payment_txn_id;
    }

    public void setOrder_payment_txn_id(String order_payment_txn_id) {
        this.order_payment_txn_id = order_payment_txn_id;
    }

    public String getOrdered_product_id() {
        return ordered_product_id;
    }

    public void setOrdered_product_id(String ordered_product_id) {
        this.ordered_product_id = ordered_product_id;
    }

    public String getOrdered_product_title() {
        return ordered_product_title;
    }

    public void setOrdered_product_title(String ordered_product_title) {
        this.ordered_product_title = ordered_product_title;
    }

    public String getOrdered_product_image() {
        return ordered_product_image;
    }

    public void setOrdered_product_image(String ordered_product_image) {
        this.ordered_product_image = ordered_product_image;
    }

    public String getOrdered_product_price() {
        return ordered_product_price;
    }

    public void setOrdered_product_price(String ordered_product_price) {
        this.ordered_product_price = ordered_product_price;
    }

    public String getOrdered_product_quantity() {
        return ordered_product_quantity;
    }

    public void setOrdered_product_quantity(String ordered_product_quantity) {
        this.ordered_product_quantity = ordered_product_quantity;
    }
}
