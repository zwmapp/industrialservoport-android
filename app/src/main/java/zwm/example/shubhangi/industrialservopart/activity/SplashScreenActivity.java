package zwm.example.shubhangi.industrialservopart.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.util.CommonApi;

import static zwm.example.shubhangi.industrialservopart.util.UserData.getUserDataFromPreference;

/**
 * Created by shubhangi on 12/5/2017.
 */

public class SplashScreenActivity  extends AppCompatActivity {
    private CommonApi commonApi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        commonApi = CommonApi.getInstance(SplashScreenActivity.this);
        getUserDataFromPreference(SplashScreenActivity.this);

        new CountDownTimer(3000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
               /* if (UserData.USER_EMAIL .equals("")) {
                    Intent in = new Intent(SplashScreenActivity.this, LoginOrSignUpActivity.class);
                    startActivity(in);
                    SplashScreenActivity.this.finish();
                } else{
                    Intent in1 = new Intent(SplashScreenActivity.this, MainActivity.class);
                    startActivity(in1);
                    SplashScreenActivity.this.finish();
                }*/
                Intent in = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(in);
                SplashScreenActivity.this.finish();
            }
        }.start();
    }
}

