package zwm.example.shubhangi.industrialservopart.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.activity.ManufacturerDetailActivity;
import zwm.example.shubhangi.industrialservopart.activity.SearchDetailManufacturerDetailActivity;
import zwm.example.shubhangi.industrialservopart.model.SearchResultModel;

/**
 * Created by shubhangi on 4/26/2018.
 */

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ViewHolder> {

    List<SearchResultModel> searchResultModelList;
    SearchResultModel searchResultModel;
    private Context context;

    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public SearchResultAdapter(Context context, List<SearchResultModel> searchResultModelList) {
        super();
        this.context = context;
        this.searchResultModelList = searchResultModelList;
    }

    // inflates the cell layout from xml when needed
    @Override
    public SearchResultAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.brand_manufacturer_recyclerview_items, parent, false);
        return new SearchResultAdapter.ViewHolder(view);
    }

    // binds the data to the textView in each cell
    @Override
    public void onBindViewHolder(SearchResultAdapter.ViewHolder holder, int position) {
        searchResultModel = searchResultModelList.get(position);

        holder.brandManufacturerPartNumber.setText(searchResultModel.getSearch_result_brand_manufacturer_part_number());
        holder.brandManufacturerDescription.setText(searchResultModel.getSearch_result_brand_manufacturer_description());
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return searchResultModelList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView brandManufacturerPartNumber, brandManufacturerDescription;

        public ViewHolder(View itemView) {
            super(itemView);
            brandManufacturerPartNumber = itemView.findViewById(R.id.brandManufacturerPartNumber);
            brandManufacturerDescription = itemView.findViewById(R.id.brandManufacturerDescription);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            int moreProductTemp = getAdapterPosition();
            Intent in = new Intent(context, SearchDetailManufacturerDetailActivity.class);
            in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle b = new Bundle();
            b.putString("manufacturerPartsID", searchResultModelList.get(getAdapterPosition()).getSearch_result_brand_manufacturer_ID());
            b.putString("manufacturerParticleId", searchResultModelList.get(getAdapterPosition()).getSearch_result_brand_part_id());
            in.putExtras(b);
            context.startActivity(in);
        }
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}






