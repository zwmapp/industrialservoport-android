package zwm.example.shubhangi.industrialservopart.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.util.Constant;
import zwm.example.shubhangi.industrialservopart.util.MyApplication;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.util.UserData;

/**
 * Created by shubhangi on 4/27/2018.
 */

public class SearchDetailManufacturerDetailActivity extends BaseActivity {
    @BindView(R.id.tvToolbarText)
    TextView tvToolbarText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

   /* @BindView(R.id.manufacturerDetailImage)
    ImageView manufacturerDetailImage;
    @BindView(R.id.manufacturerDetailTitle)
    TextView manufacturerDetailTitle;
    @BindView(R.id.manufacturerDetailDescription)
    TextView manufacturerDetailDescription;*/

    String manufacturerPartsID,manufacturerParticleId;
    String category, ManufacturerString, Part_No, DescriptionString;
    @BindView(R.id.Category)
    TextView Category;
    @BindView(R.id.Manufacturer)
    TextView Manufacturer;
    @BindView(R.id.ModelNo)
    TextView ModelNo;
    @BindView(R.id.Description)
    TextView Description;
    @BindView(R.id.someMoreInfo)
    TextView someMoreInfo;
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etEmailAddress)
    EditText etEmailAddress;
    @BindView(R.id.etPhone)
    EditText etPhone;
    @BindView(R.id.etComment)
    EditText etComment;

    @BindView(R.id.quoteLayout)
    LinearLayout quoteLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manufacturer_detail);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tvToolbarText.setText("Manufacturer Detail");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Bundle b = getIntent().getExtras();
        manufacturerPartsID = b.getString("manufacturerPartsID");
        Log.d(TAG, "manufacturerPartsID" + manufacturerPartsID);
        manufacturerParticleId = b.getString("manufacturerParticleId");
        Log.d(TAG, "manufacturerParticleId" + manufacturerParticleId);

        etName.setText(UserData.USER_FULL_NAME);
        etEmailAddress.setText(UserData.USER_EMAIL);
        etPhone.setText(UserData.USER_PHONE);

        manufacturerDetail();
    }

    private void manufacturerDetail() {
        showProgressDialog("Please wait..");
        StringRequest stringRequestNew = new StringRequest(Request.Method.POST, Urls.ALPHABETICAL_BRAND_MANUFACTURER_LISTINGS_DEATIL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "responseS" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                JSONObject jsonObjItems = jsonObject.getJSONObject("response");

                                category = jsonObjItems.getString("Category");
                                ManufacturerString = jsonObjItems.getString("Manufacturer");
                                Part_No = jsonObjItems.getString("Part_No");
                                DescriptionString = jsonObjItems.getString("Description");

                                Category.setText("Category/Type: " + category);
                                Manufacturer.setText("Manufacturer: " + ManufacturerString);
                                ModelNo.setText("Model/Part No.: " + Part_No);
                                Description.setText("Description: " + DescriptionString);
                                someMoreInfo.setText("All our highly trained technicians are ready to repair and refurbish your " +  ManufacturerString  + " Product Number " +  Part_No);

                               /* Glide.with(ManufacturerDetailActivity.this)
                                        .load(jsonObjectItems.getString("image"))
                                        .into(manufacturerDetailImage);
                                manufacturerDetailTitle.setText(jsonObjectItems.getString("title"));
                                manufacturerDetailDescription.setText(jsonObjectItems.getString("description"));*/

                            } else {
                                String loginError = jsonObject.getJSONObject("response").getString("msg_for_user");
                                Toast.makeText(SearchDetailManufacturerDetailActivity.this, loginError, Toast.LENGTH_SHORT).show();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "ERROR_LOGIN: " + e.getMessage());
                        }
                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "error" + error);
                        Toast.makeText(SearchDetailManufacturerDetailActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("manid", manufacturerPartsID);
                params.put("parid", manufacturerParticleId);
                Log.d(TAG, "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);
    }

    @OnClick(R.id.submitButton)
    public void onViewClicked() {
        commonApi.errorMsg = "";
        errorMsg = commonApi.checkRequiredFields(quoteLayout);
        if (errorMsg.equals("")) {
            uploadRequestQuote();
        } else {
            commonApi.showSnackBar(quoteLayout, "" + errorMsg, 1);
            return;
        }
    }

    private void uploadRequestQuote() {
        if (!commonApi.checkEmailValidation(etEmailAddress.getText().toString().trim())) {
            commonApi.showSnackBar(quoteLayout, Constant.EMAIL_VALIDATION + errorMsg, 1);
            return;
        }
        showProgressDialog("Please wait..");
        StringRequest stringRequestNew = new StringRequest(Request.Method.POST, Urls.MANUFACTURER_REQUEST_QUOTE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "responseS" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                JSONObject jsonObjectItemsSignUp = jsonObject.getJSONObject("response");
                                Toast.makeText(SearchDetailManufacturerDetailActivity.this, "Thank you for contacting us with the Quote request. We will be in touch with you very soon.", Toast.LENGTH_SHORT).show();
                                SearchDetailManufacturerDetailActivity.this.finish();

                            } else {
                                String loginError = jsonObject.getJSONObject("response").getString("msg_for_user");
                                Toast.makeText(SearchDetailManufacturerDetailActivity.this, loginError, Toast.LENGTH_SHORT).show();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "ERROR_LOGIN: " + e.getMessage());
                        }
                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SearchDetailManufacturerDetailActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("manufacturer_id", UserData.BRAND_VALUE_FOR_ALPHABET_MANUFACTURER);
                params.put("name", etName.getText().toString().trim());
                params.put("email", etEmailAddress.getText().toString().trim());
                params.put("phone", etPhone.getText().toString().trim());
                params.put("message", etComment.getText().toString().trim());
                params.put("product_name", manufacturerPartsID);
                params.put("contactUs", "1");
                Log.d(TAG, "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);
    }
}

