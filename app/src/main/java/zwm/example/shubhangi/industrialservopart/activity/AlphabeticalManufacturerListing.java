package zwm.example.shubhangi.industrialservopart.activity;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.adapter.AlphabeticListingAdapter;
import zwm.example.shubhangi.industrialservopart.adapter.AlphabeticalManufacturerListingAdapter;
import zwm.example.shubhangi.industrialservopart.model.AlphaBeticModel;
import zwm.example.shubhangi.industrialservopart.model.AlphabeticalManufacturerListingModel;
import zwm.example.shubhangi.industrialservopart.util.MyApplication;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.util.UserData;

/**
 * Created by shubhangi on 1/20/2018.
 */

public class AlphabeticalManufacturerListing extends BaseActivity {
    @BindView(R.id.tvToolbarText)
    TextView tvToolbarText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerViewAlphabeticalManufacturerListing)
    RecyclerView recyclerViewAlphabeticalManufacturerListing;
    List<AlphabeticalManufacturerListingModel> alphabeticalManufacturerListingModelList;
    AlphabeticalManufacturerListingAdapter alphabeticalManufacturerListingAdapter;
    String alphabets;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alphabetical_manufacturer_listing);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (UserData.SERVO_MOTOR_REPAIR_BUTTON != null) {
            tvToolbarText.setText("Servo Motors Repair");
        } else {
            tvToolbarText.setText("Industrial Monitors Repair");

        }
        //tvToolbarText.setText("Supported Manufacturers");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        Bundle b = getIntent().getExtras();
        alphabets = b.getString("alphabets");
        Log.d(TAG, "alphabets" + alphabets);
        alphabeticalManufacturerListingModelList = new ArrayList<>();
        showAlphabeticalManufacturers();
    }

    private void showAlphabeticalManufacturers() {
        showProgressDialog("Please wait..");
        StringRequest stringRequestNew = new StringRequest(Request.Method.POST, Urls.ALPHABETICAL_MANUFACTURER_LISTING,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "responseS" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                JSONArray jsonArrayItems = jsonObject.getJSONArray("response");
                                for (int i = 0; i < jsonArrayItems.length(); i++) {
                                    AlphabeticalManufacturerListingModel alphabeticalManufacturerListingModel = new AlphabeticalManufacturerListingModel();
                                    try {
                                        JSONObject rec = jsonArrayItems.getJSONObject(i);
                                        alphabeticalManufacturerListingModel.setAlphabetical_manufacturer_listing_id(rec.getString("ID"));
                                        alphabeticalManufacturerListingModel.setAlphabetical_manufacturer_listing_title(rec.getString("title"));

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    dismissProgressDialog();
                                    alphabeticalManufacturerListingModelList.add(alphabeticalManufacturerListingModel);

                                }
                                alphabeticalManufacturerListingAdapter = new AlphabeticalManufacturerListingAdapter(AlphabeticalManufacturerListing.this, alphabeticalManufacturerListingModelList);
                                recyclerViewAlphabeticalManufacturerListing.setAdapter(alphabeticalManufacturerListingAdapter);

                            } else {
                                String loginError = jsonObject.getJSONObject("response").getString("msg_for_user");
                                Toast.makeText(AlphabeticalManufacturerListing.this, loginError, Toast.LENGTH_SHORT).show();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "ERROR_LOGIN: " + e.getMessage());
                        }
                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "error" + error);
                        Toast.makeText(AlphabeticalManufacturerListing.this, error.toString(), Toast.LENGTH_LONG).show();
                        dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (UserData.SERVO_MOTOR_REPAIR_BUTTON != null) {
                    params.put("ct", "1");
                    UserData.CT_VALUE = "1";
                } else {
                    params.put("ct", "7");
                    UserData.CT_VALUE = "7";
                }
                params.put("man", alphabets);
                Log.d(TAG, "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);
    }
}

