package zwm.example.shubhangi.industrialservopart.model;

/**
 * Created by shubhangi on 1/20/2018.
 */

public class AlphabeticalManufacturerListingModel {
    private String alphabetical_manufacturer_listing_id;
    private String alphabetical_manufacturer_listing_title;

    public String getAlphabetical_manufacturer_listing_id() {
        return alphabetical_manufacturer_listing_id;
    }

    public void setAlphabetical_manufacturer_listing_id(String alphabetical_manufacturer_listing_id) {
        this.alphabetical_manufacturer_listing_id = alphabetical_manufacturer_listing_id;
    }

    public String getAlphabetical_manufacturer_listing_title() {
        return alphabetical_manufacturer_listing_title;
    }

    public void setAlphabetical_manufacturer_listing_title(String alphabetical_manufacturer_listing_title) {
        this.alphabetical_manufacturer_listing_title = alphabetical_manufacturer_listing_title;
    }
}
