package zwm.example.shubhangi.industrialservopart.activity;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.drakeet.materialdialog.MaterialDialog;
import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.util.Constant;
import zwm.example.shubhangi.industrialservopart.util.MyApplication;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.util.UserData;

/**
 * Created by shubhangi on 1/6/2018.
 */

public class EbaySubProductDetailDescriptionActivity extends BaseActivity {
    @BindView(R.id.tvToolbarText)
    TextView tvToolbarText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.productDetailImage)
    ImageView productDetailImage;
    @BindView(R.id.productNameTextView)
    TextView productNameTextView;
    @BindView(R.id.productPriceTextView)
    TextView productPriceTextView;
    @BindView(R.id.productDetailTextView)
    TextView productDetailTextView;
    @BindView(R.id.seeMore)
    TextView seeMore;

    String ebayId, ebaySubProductID;
    //dialog
    MaterialDialog materialDialog;
    EditText makeAnOfferName, makeAnOfferEmail, makeAnOfferPhone, makeAnOfferMessage;
    LinearLayout makeAnOfferLayout;
    TextView textViewProductTitle;
    String productTitleForOffer;
    //dialog

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ebay_sub_productdetail_description);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tvToolbarText.setText("Product Description");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        Bundle b = getIntent().getExtras();
        ebayId = b.getString("ebayId");
        ebaySubProductID = b.getString("ebaySubProductID");
        Log.d(TAG, "ebayId" + ebayId);

        ebayProductDetailDescription();

        seeMore.setVisibility(View.VISIBLE);
    }

    private void ebayProductDetailDescription() {
        showProgressDialog("Please wait..");
        StringRequest stringRequestNew = new StringRequest(Request.Method.POST, Urls.EBAY_CATEGORIES_SUB_PRODUCT_DETAIL_DESCRIPTION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "responseS" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                JSONObject jsonObjectItems = jsonObject.getJSONObject("response");

                                Glide.with(EbaySubProductDetailDescriptionActivity.this)
                                        .load(jsonObjectItems.getString("image"))
                                        .into(productDetailImage);
                                productNameTextView.setText(jsonObjectItems.getString("title"));
                                productNameTextView.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
                                productTitleForOffer = jsonObjectItems.getString("title");
                                productPriceTextView.setText("$" + jsonObjectItems.getString("price"));
                                productDetailTextView.setText(jsonObjectItems.getString("description"));
                                productDetailTextView.setMaxLines(4);


                            } else {
                                String loginError = jsonObject.getJSONObject("response").getString("msg_for_user");
                                Toast.makeText(EbaySubProductDetailDescriptionActivity.this, loginError, Toast.LENGTH_SHORT).show();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "ERROR_LOGIN: " + e.getMessage());
                        }
                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "error" + error);
                        Toast.makeText(EbaySubProductDetailDescriptionActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("item_id", ebayId);
                Log.d(TAG, "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);
    }

    @OnClick({R.id.makeAnOffer, R.id.addToCartButton,R.id.seeMore})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.makeAnOffer:
                openDialogMethod();
                break;
            case R.id.seeMore:
                seeMoreMethod();
                break;
            case R.id.addToCartButton:
                if (UserData.LOGGED_IN) {
                    addToCartMethod();
                } else {
                    Toast.makeText(EbaySubProductDetailDescriptionActivity.this, "Please log in to add item in cart.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void seeMoreMethod() {
        seeMore.setVisibility(View.GONE);
        productDetailTextView.setMaxLines(400);
    }
    private void addToCartMethod() {
        showProgressDialog("Please Wait..");
        StringRequest stringRequestNew = new StringRequest(com.android.volley.Request.Method.POST, Urls.ADD_TO_CART,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        Log.d("TAG", "response" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                Toast.makeText(EbaySubProductDetailDescriptionActivity.this, "Please check your cart", Toast.LENGTH_LONG).show();
                                JSONObject jsonObjectItemsAddToCart = jsonObject.getJSONObject("response");

                                finalCartCount = jsonObjectItemsAddToCart.getInt("count_product");
                                /*activity.*/
                                refreshCartCounter();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("TAG", "ERROR_LOGIN: " + e.getMessage());
                        }
                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EbaySubProductDetailDescriptionActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", UserData.USER_ID);
                params.put("product_id", ebaySubProductID);
                params.put("qty", "1");
                Log.d("TAG", "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);
    }

    private void openDialogMethod() {
        View view1 = LayoutInflater.from(this).inflate(R.layout.make_an_offer_dialog, null);
        materialDialog = new MaterialDialog(this).setContentView(view1);
        makeAnOfferName = view1.findViewById(R.id.makeAnOfferName);
        makeAnOfferEmail = view1.findViewById(R.id.makeAnOfferEmail);
        makeAnOfferPhone = view1.findViewById(R.id.makeAnOfferPhone);
        makeAnOfferMessage = view1.findViewById(R.id.makeAnOfferMessage);
        makeAnOfferLayout = view1.findViewById(R.id.makeAnOfferLayout);
        textViewProductTitle = view1.findViewById(R.id.textViewProductTitle);

        makeAnOfferName.setText(UserData.USER_FULL_NAME);
        makeAnOfferEmail.setText(UserData.USER_EMAIL);
        makeAnOfferPhone.setText(UserData.USER_PHONE);
        textViewProductTitle.setText(productTitleForOffer);


        ImageView imageCloseIcon = view1.findViewById(R.id.imageCloseIcon);
        imageCloseIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                materialDialog.dismiss();
            }
        });

        Button buttonMakeAnOfferSubmit = view1.findViewById(R.id.buttonMakeAnOfferSubmit);
        buttonMakeAnOfferSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commonApi.errorMsg = "";
                errorMsg = commonApi.checkRequiredFields(makeAnOfferLayout);
                if (errorMsg.equals("")) {
                    uploadMakeAnOfferData();
                } else {
                    commonApi.showSnackBar(makeAnOfferLayout, "" + errorMsg, 1);
                    return;
                }
            }
        });


        materialDialog.show();
    }

    private void uploadMakeAnOfferData() {
        if (!commonApi.checkEmailValidation(makeAnOfferEmail.getText().toString().trim())) {
            commonApi.showSnackBar(makeAnOfferLayout, Constant.EMAIL_VALIDATION + errorMsg, 1);
            return;
        }
        showProgressDialog("Please wait..");
        StringRequest stringRequestNew = new StringRequest(Request.Method.POST, Urls.MAKE_AN_OFFER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "responseS" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                JSONObject jsonObjectItemsLogin = jsonObject.getJSONObject("response");
                                // commonApi.openNewScreen(MainActivity.class, null);
                                Toast.makeText(EbaySubProductDetailDescriptionActivity.this, "Thank you for making an offer with us. We will be in touch with you very soon.", Toast.LENGTH_SHORT).show();
                                materialDialog.dismiss();
                            } else {
                                String loginError = jsonObject.getJSONObject("response").getString("msg_for_user");
                                Toast.makeText(EbaySubProductDetailDescriptionActivity.this, loginError, Toast.LENGTH_SHORT).show();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "ERROR_LOGIN: " + e.getMessage());
                        }
                        dismissProgressDialog();
                        materialDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "error" + error);
                        Toast.makeText(EbaySubProductDetailDescriptionActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        dismissProgressDialog();
                        materialDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", UserData.USER_ID);
                params.put("product_id", ebayId);
                params.put("name", makeAnOfferName.getText().toString().trim());
                params.put("email", makeAnOfferEmail.getText().toString().trim());
                params.put("telephone", makeAnOfferPhone.getText().toString().trim());
                params.put("comments", makeAnOfferMessage.getText().toString().trim());
                params.put("contactUs1", "1");
                Log.d(TAG, "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);
    }

}


