package zwm.example.shubhangi.industrialservopart.alertdialog;

import android.content.DialogInterface;



public interface IDialogs {
    void positiveClick(DialogInterface dialog, int id);
    void negativeClick(DialogInterface dialog, int id);
}
