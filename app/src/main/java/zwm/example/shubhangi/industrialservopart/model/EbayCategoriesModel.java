package zwm.example.shubhangi.industrialservopart.model;

/**
 * Created by shubhangi on 1/5/2018.
 */

public class EbayCategoriesModel {
    private String ebay_product_id;
    private String ebay_product_title;
    private String ebay_product_image;
    private String ebay_product_description;
    private String ebay_product_no_of_products;

    public String getEbay_product_id() {
        return ebay_product_id;
    }

    public void setEbay_product_id(String ebay_product_id) {
        this.ebay_product_id = ebay_product_id;
    }

    public String getEbay_product_title() {
        return ebay_product_title;
    }

    public void setEbay_product_title(String ebay_product_title) {
        this.ebay_product_title = ebay_product_title;
    }

    public String getEbay_product_image() {
        return ebay_product_image;
    }

    public void setEbay_product_image(String ebay_product_image) {
        this.ebay_product_image = ebay_product_image;
    }

    public String getEbay_product_description() {
        return ebay_product_description;
    }

    public void setEbay_product_description(String ebay_product_description) {
        this.ebay_product_description = ebay_product_description;
    }

    public String getEbay_product_no_of_products() {
        return ebay_product_no_of_products;
    }

    public void setEbay_product_no_of_products(String ebay_product_no_of_products) {
        this.ebay_product_no_of_products = ebay_product_no_of_products;
    }


}
