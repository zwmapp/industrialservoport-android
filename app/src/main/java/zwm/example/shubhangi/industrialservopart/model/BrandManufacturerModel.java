package zwm.example.shubhangi.industrialservopart.model;

/**
 * Created by shubhangi on 1/8/2018.
 */

public class BrandManufacturerModel {
    private String barnd_manufacturer_id;
    private String brand__manufacturer_part_number;
    private String brand_manufacturer_description;
    private String brand_Parts_id;

    public String getBarnd_manufacturer_id() {
        return barnd_manufacturer_id;
    }

    public void setBarnd_manufacturer_id(String barnd_manufacturer_id) {
        this.barnd_manufacturer_id = barnd_manufacturer_id;
    }

    public String getBrand__manufacturer_part_number() {
        return brand__manufacturer_part_number;
    }

    public void setBrand__manufacturer_part_number(String brand__manufacturer_part_number) {
        this.brand__manufacturer_part_number = brand__manufacturer_part_number;
    }

    public String getBrand_manufacturer_description() {
        return brand_manufacturer_description;
    }

    public void setBrand_manufacturer_description(String brand_manufacturer_description) {
        this.brand_manufacturer_description = brand_manufacturer_description;
    }

    public String getBrand_Parts_id() {
        return brand_Parts_id;
    }

    public void setBrand_Parts_id(String brand_Parts_id) {
        this.brand_Parts_id = brand_Parts_id;
    }
}
