package zwm.example.shubhangi.industrialservopart.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.activity.EbaySubProductDetailActivity;
import zwm.example.shubhangi.industrialservopart.model.EbayCategoriesModel;

/**
 * Created by shubhangi on 1/5/2018.
 */

public class EbayCategoriesAdapter extends RecyclerView.Adapter<EbayCategoriesAdapter.ViewHolder> {

    List<EbayCategoriesModel> ebayCategoriesModelList;
    EbayCategoriesModel ebayCategoriesModel;
    private Context context;

    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public EbayCategoriesAdapter(Context context, List<EbayCategoriesModel> ebayCategoriesModelList) {
        super();
        this.context = context;
        this.ebayCategoriesModelList = ebayCategoriesModelList;
    }

    // inflates the cell layout from xml when needed
    @Override
    public EbayCategoriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ebay_categories_recyclerview_items, parent, false);
        return new EbayCategoriesAdapter.ViewHolder(view);
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(EbayCategoriesAdapter.ViewHolder holder, int position) {
        ebayCategoriesModel = ebayCategoriesModelList.get(position);

        holder.ebayCategoriesTitle.setText(ebayCategoriesModel.getEbay_product_title());
        holder.ebayCategoriesNumberOfProduct.setText("(" + ebayCategoriesModel.getEbay_product_no_of_products() + ")");
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return ebayCategoriesModelList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView ebayCategoriesTitle, ebayCategoriesNumberOfProduct;

        public ViewHolder(View itemView) {
            super(itemView);
            ebayCategoriesTitle = itemView.findViewById(R.id.ebayCategoriesTitle);
            ebayCategoriesNumberOfProduct = itemView.findViewById(R.id.ebayCategoriesNumberOfProduct);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            int moreProductTemp = getAdapterPosition();
            Intent in = new Intent(context, EbaySubProductDetailActivity.class);
            in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle b = new Bundle();
            b.putString("ebayProductItemPositionTitle", ebayCategoriesModelList.get(getAdapterPosition()).getEbay_product_title());
            b.putInt("ebayProductItemPosition", Integer.parseInt(ebayCategoriesModelList.get(getAdapterPosition()).getEbay_product_id()));
            in.putExtras(b);
            context.startActivity(in);
        }
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}




