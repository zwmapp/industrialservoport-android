package zwm.example.shubhangi.industrialservopart.activity;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.adapter.CartItemAdapter;
import zwm.example.shubhangi.industrialservopart.model.CartItemModel;
import zwm.example.shubhangi.industrialservopart.util.CartUpdateListener;
import zwm.example.shubhangi.industrialservopart.util.MyApplication;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.util.UserData;

/**
 * Created by shubhangi on 1/11/2018.
 */

public class CartItemActivity extends BaseActivity implements CartUpdateListener {

    @BindView(R.id.tvToolbarText)
    TextView tvToolbarText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerViewMyCartItem)
    RecyclerView recyclerViewMyCartItem;
    @BindView(R.id.tv_alert)
    TextView tv_alert;

    @BindView(R.id.editTotalBill)
    TextView editTotalBill;


    // Cart
    List<CartItemModel> cartItemModelList;
    CartItemAdapter cartItemAdapter;
    // Cart

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_item);
        ButterKnife.bind(this);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tvToolbarText.setText("My Cart");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        //cart
        cartItemModelList = new ArrayList<>();
        recyclerViewMyCartItem.setHasFixedSize(true);
        //cart
        Log.d("TAGGG", UserData.CART_TOTAL_BILL);
        editTotalBill.setText("$" + UserData.CART_TOTAL_BILL);

        getCartMethod();

    }

    private void getCartMethod() {
        showProgressDialog("Please Wait..");
        StringRequest stringRequestNew = new StringRequest(Request.Method.POST, Urls.GET_CART,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "responseS" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                JSONObject jsonObjectResponse = jsonObject.getJSONObject("response");
                                JSONArray jsonObjectCartItemsArray = jsonObjectResponse.getJSONArray("items");

                                Log.d(TAG, "jsonObjectCartItemsArray" + jsonObjectCartItemsArray);
                                UserData.CART_TOTAL_BILL = jsonObjectResponse.getString("total");
                                editTotalBill.setText("$" + UserData.CART_TOTAL_BILL);
                                finalCartCount = jsonObjectResponse.getInt("count_product");
                                refreshCartCounter();
                                // JSONArray jsonObjectCartItems = jsonObjectCartItemsObj.getJSONArray("0");

                                for (int j = 0; j < jsonObjectCartItemsArray.length(); j++) {
                                    CartItemModel cartItemModel = new CartItemModel();
                                    try {
                                        JSONObject jsonObj = jsonObjectCartItemsArray.getJSONObject(j);
                                        cartItemModel.setCart_item_id(jsonObj.getString("ID"));
                                        UserData.PRODUCT_ID_IN_CART = jsonObj.getString("ID");
                                        cartItemModel.setCart_item_title(jsonObj.getString("title"));
                                        cartItemModel.setCart_item_image(jsonObj.getString("image"));
                                        cartItemModel.setCart_item_price(jsonObj.getString("price"));
                                        cartItemModel.setCart_item_quantity(jsonObj.getString("quantity"));
                                        cartItemModel.setCart_item_updated_price(jsonObj.getString("sub_total"));
                                        cartItemModel.setCart_item_SKU_number(jsonObj.getString("SKU"));


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    cartItemModelList.add(cartItemModel);
                                }
                                if (cartItemModelList.size() > 0) {
                                    recyclerViewMyCartItem.setVisibility(View.VISIBLE);
                                    tv_alert.setVisibility(View.GONE);
                                } else {
                                    recyclerViewMyCartItem.setVisibility(View.GONE);
                                    tv_alert.setVisibility(View.VISIBLE);
                                }
                                cartItemAdapter = new CartItemAdapter(CartItemActivity.this, cartItemModelList);
                                cartItemAdapter.addObsever(CartItemActivity.this);
                                recyclerViewMyCartItem.setAdapter(cartItemAdapter);



                            }
                            else
                            {
                                editTotalBill.setText("$" + "0");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "ERROR_LOGIN: " + e.getMessage());
                        }
                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CartItemActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", UserData.USER_ID);
                Log.d(TAG, "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);
    }


    @OnClick({R.id.buttonUpdateCart, R.id.buttonCheckout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonUpdateCart:
                if (cartItemModelList.size() > 0) {
                    showProgressDialog("Please Wait..");
                    for (int j = 0; j < cartItemModelList.size(); j++) {
                        final CartItemModel cartItemModelForPost = cartItemModelList.get(j);
                        StringRequest stringRequestNew = new StringRequest(com.android.volley.Request.Method.POST, Urls.UPDATE_CART,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(final String response) {
                                        Log.d("TAG", "response" + response);
                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                                Toast.makeText(CartItemActivity.this, "Updated", Toast.LENGTH_LONG).show();
                                                JSONObject getJsonObject = jsonObject.getJSONObject("response");
                                                UserData.CART_TOTAL_BILL = getJsonObject.getString("total");
                                                editTotalBill.setText("$" + UserData.CART_TOTAL_BILL);
                                                JSONArray jsonArrayItems = getJsonObject.getJSONArray("items");
                                                cartItemModelList = new ArrayList<>();
                                                for (int k = 0; k < jsonArrayItems.length(); k++) {
                                                    CartItemModel cartItemModel1 = new CartItemModel();
                                                    try {
                                                        JSONObject jsonObj = jsonArrayItems.getJSONObject(k);

                                                        cartItemModel1.setCart_item_id(jsonObj.getString("ID"));
                                                        cartItemModel1.setCart_item_price(jsonObj.getString("price"));
                                                        cartItemModel1.setCart_item_title(jsonObj.getString("title"));
                                                        cartItemModel1.setCart_item_image(jsonObj.getString("image"));
                                                        cartItemModel1.setCart_item_quantity(jsonObj.getString("quantity"));
                                                        cartItemModel1.setCart_item_updated_price(jsonObj.getString("sub_total"));
                                                        //cartItemModel1.setTotal_price_cart(jsonObj.getString("total_price"));

                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                    cartItemModelList.add(cartItemModel1);
                                                }
                                                cartItemAdapter = new CartItemAdapter(CartItemActivity.this, cartItemModelList);
                                                cartItemAdapter.addObsever(CartItemActivity.this);
                                                recyclerViewMyCartItem.setAdapter(cartItemAdapter);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            Log.e("TAG", "ERROR_LOGIN: " + e.getMessage());
                                        }
                                        dismissProgressDialog();
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(CartItemActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                                        dismissProgressDialog();
                                    }
                                }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<>();
                                params.put("user_id", UserData.USER_ID);
                                params.put("product_id", cartItemModelForPost.getCart_item_id());
                                params.put("qty", cartItemModelForPost.getCart_item_quantity());
                                Log.d("TAG", "params" + params);
                                return params;
                            }
                        };
                        MyApplication.getInstance().addToRequestQueue(stringRequestNew);
                    }
                } else {
                    Toast.makeText(this, "Please add some item on cart", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.buttonCheckout:
                if (cartItemModelList.size() > 0) {
                    commonApi.openNewScreen(CheckOutActivity.class, null);
                    CartItemActivity.this.finish();

                } else {
                    Toast.makeText(this, "Please add some item on cart", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    @Override
    public void updateCartValue(String cartTotalPrice) {
        editTotalBill.setText("$" + cartTotalPrice);
        Log.e("UPdate ",cartTotalPrice);
    }
}
