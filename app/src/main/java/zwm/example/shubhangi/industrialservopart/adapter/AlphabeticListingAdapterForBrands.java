package zwm.example.shubhangi.industrialservopart.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.activity.BrandManufacturerActivity;
import zwm.example.shubhangi.industrialservopart.util.UserData;

/**
 * Created by shubhangi on 1/29/2018.
 */

public class AlphabeticListingAdapterForBrands extends RecyclerView.Adapter<AlphabeticListingAdapterForBrands.MyViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    String alphaId[];
    private ItemClickListener mClickListener;

    public AlphabeticListingAdapterForBrands(Context context, String alphaId[]) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.alphaId = alphaId;
    }

    public void delete(int position) {
        // data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.alphabetic_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.alphabeticImage.setText(alphaId[position]);
    }

    @Override
    public int getItemCount() {
        return alphaId.length;
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView alphabeticImage;

        MyViewHolder(View itemView) {
            super(itemView);
            alphabeticImage = itemView.findViewById(R.id.alphabeticImage);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            int moreProductTemp = getAdapterPosition();
            String text = alphaId[getLayoutPosition()].toString();
            Toast.makeText(context, "" + text, Toast.LENGTH_SHORT).show();
            UserData.ALPHABET_VALUE = text;
            Intent in = new Intent(context, BrandManufacturerActivity.class);
            in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle bundleForAlphabets = new Bundle();
            bundleForAlphabets.putString("alphabetsForBrandManufacturer", alphaId[getLayoutPosition()].toString());
            in.putExtras(bundleForAlphabets);
            //context.startActivity(in);
        }
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}


