package zwm.example.shubhangi.industrialservopart.alertdialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import zwm.example.shubhangi.industrialservopart.R;


public abstract class DialogBoxFactory implements IDialogs {

    private static final String TAG = DialogBoxFactory.class.getCanonicalName();

    public static int TYPE_SINGLE_BUTTON = 0;
    public static int TYPE_DOUBLE_BUTTON = 1;
    public int type = 0;
    String title, message, pName, nName;
    Context ctx;

    public abstract void positiveClick(DialogInterface dialog, int id);

    public abstract void negativeClick(DialogInterface dialog, int id);

    public DialogBoxFactory(String title, String message, Context ctx, int type, String pName) {
        super();
        this.title = title;
        this.message = message;
        this.ctx = ctx;
        this.type = type;
        this.pName = pName;
    }

    public DialogBoxFactory(String title, String message, Context ctx, int type, String pName, String nName) {
        super();
        this.title = title;
        this.message = message;
        this.ctx = ctx;
        this.type = type;
        this.pName = pName;
        this.nName = nName;

    }

    public synchronized final DialogBoxFactory start() {
        call();
        return this;
    }

    public void call() {
        switch (type) {
            case 0:
                AlertDialog.Builder _builder = new AlertDialog.Builder(ctx,R.style.AlertDialog);
                _builder.setTitle(title);
                _builder.setMessage(message)
                        .setCancelable(false)
                        .setPositiveButton(pName, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                positiveClick(dialog, id);
                            }
                        });
                _builder.create();
                _builder.show();
                break;

            case 1:
                /**
                 * Create a simple dialog box with OK button and a message.
                 *
                 * @param message - Message to show.
                 * @param ctx     - Context on which dialog will be shown.
                 * @return Alert dialog created.
                 */
                AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                builder.setTitle(title);
                builder.setMessage(message)
                        .setCancelable(false)
                        .setPositiveButton(pName, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                positiveClick(dialog, id);

                            }
                        }).setNegativeButton(nName, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        negativeClick(dialog, which);
                    }
                });
                builder.create();
                builder.show();
                break;
        }
    }
}
