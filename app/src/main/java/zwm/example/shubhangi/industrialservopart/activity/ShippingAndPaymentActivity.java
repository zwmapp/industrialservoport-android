package zwm.example.shubhangi.industrialservopart.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.volleyparser.GetServiceCall;

/**
 * Created by shubhangi on 4/23/2018.
 */

public class ShippingAndPaymentActivity extends BaseActivity {
    
    String UrlForShippingDetails = "https://repairservomotors.com/webservice/api.php?action=detail_item&item_id=192344349137";
    @BindView(R.id.tvToolbarText)
    TextView tvToolbarText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.sp_country)
    Spinner sp_country;
    String countryName;

    private ArrayList<String> country_name = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping_and_payment);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tvToolbarText.setText("Shipping And Payments");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        
        shippingDetailMethod();
    }

    private void shippingDetailMethod() {
        showProgressDialog("Please wait..");
        new GetServiceCall(UrlForShippingDetails, GetServiceCall.TYPE_JSONOBJECT) {
            @Override
            public void response(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsoO = jsonObject.getJSONObject("response");
                    JSONArray jsonArrayOfCountry = jsoO.getJSONArray("country_list");
                    Log.d(TAG,"jsonArrayOfCountry" +jsonArrayOfCountry);
                    country_name.add("Please select country");

                    for (int i = 0; i < jsonArrayOfCountry.length(); i++) {
                        try {
                            String country=jsonArrayOfCountry.getString(i);
                            country_name.add(country);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    //setting adapter
                    ArrayAdapter<String> countryNameAdapter = new ArrayAdapter<String>(ShippingAndPaymentActivity.this, R.layout.spinner_item, R.id.textview, country_name);
                    sp_country.setAdapter(countryNameAdapter);
                    sp_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            countryName = sp_country.getSelectedItem().toString();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
                dismissProgressDialog();

            }

            @Override
            public void error(VolleyError error, String errorMsg) {
                Log.d(TAG, "error: " + error.toString());
                Log.d(TAG, "errorMsg: " + errorMsg.toString());
                dismissProgressDialog();
            }
        }.call();

    }
}
