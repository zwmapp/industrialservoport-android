package zwm.example.shubhangi.industrialservopart.util;

/**
 * Created by shubhangi on 12/7/2017.
 */

public class Constant {
    public static final String NO_INTERNET = "No internet connection!";


    public static final String EMAIL_VALIDATION = "Please enter a valid email address.";
    public static final String PASSWORD_LENGTH_CHECKING="Password length must be at-least six characters long.";
}
