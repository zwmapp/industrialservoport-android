package zwm.example.shubhangi.industrialservopart.activity;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.adapter.AlphabeticListingAdapter;
import zwm.example.shubhangi.industrialservopart.adapter.AlphabeticListingAdapterForBrands;
import zwm.example.shubhangi.industrialservopart.adapter.BrandManufacturerAdapter;
import zwm.example.shubhangi.industrialservopart.model.BrandManufacturerModel;
import zwm.example.shubhangi.industrialservopart.util.MyApplication;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.util.UserData;

/**
 * Created by shubhangi on 1/8/2018.
 */

public class BrandManufacturerActivity extends BaseActivity {
    @BindView(R.id.tvToolbarText)
    TextView tvToolbarText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerViewAlphabetList)
    RecyclerView recyclerViewAlphabetList;
    @BindView(R.id.recyclerViewManufacturer)
    RecyclerView recyclerViewManufacturer;
    int brandID;

    List<BrandManufacturerModel> brandManufacturerModelList;
    int numberOfColumns = 2;
    BrandManufacturerAdapter brandManufacturerAdapter;
    int pageNo = 1;

    //for alphabetList
    public String[] alphaBeticList = {"[0-9]", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    private AlphabeticListingAdapterForBrands alphabeticListingAdapterForBrands;
    int numberOfColumnsAlphabet = 7;
    String alphabetsForBrandsManufacturer;
    //for alphabetList


    String ManIdFromCt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_manufacturer);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tvToolbarText.setText("Supported Manufacturers");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        Bundle b = getIntent().getExtras();
        brandID = b.getInt("brandId");
        Log.d(TAG, "brandID" + brandID);

        Bundle b1 = getIntent().getExtras();
        ManIdFromCt = b1.getString("manIDFromCT");
        Log.d(TAG, "ManIdFromCt" + ManIdFromCt);

        //Brand Manufacturer
        brandManufacturerModelList = new ArrayList<>();
        recyclerViewManufacturer.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        recyclerViewManufacturer.setHasFixedSize(true);
        //Brand Manufacturer

        //for alphabetList
        recyclerViewAlphabetList.setLayoutManager(new GridLayoutManager(this, numberOfColumnsAlphabet));
        alphabeticListingAdapterForBrands = new AlphabeticListingAdapterForBrands(BrandManufacturerActivity.this, alphaBeticList);
        recyclerViewAlphabetList.setAdapter(alphabeticListingAdapterForBrands);
        alphabeticListingAdapterForBrands.setClickListener(new AlphabeticListingAdapterForBrands.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                String mainAlphabetText = alphaBeticList[position];
                UserData.ALPHABET_VALUE = mainAlphabetText;
                Toast.makeText(BrandManufacturerActivity.this, mainAlphabetText, Toast.LENGTH_SHORT).show();
                getManufacturerDetail();
            }
        });
        //for alphabetList


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            alphabetsForBrandsManufacturer = bundle.getString("alphabetsForBrandManufacturer");
        }
        Log.d(TAG, "alphabets" + alphabetsForBrandsManufacturer);
        getManufacturerDetail();
    }

    private void getManufacturerDetail() {
        showProgressDialog("Please wait..");
        StringRequest stringRequestNew = new StringRequest(Request.Method.POST, Urls.ALPHABETICAL_BRAND_MANUFACTURER_LISTINGS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "responseS" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                brandManufacturerModelList = new ArrayList<>();
                                JSONArray jsonArrayItems = jsonObject.getJSONArray("response");
                                for (int i = 0; i < jsonArrayItems.length(); i++) {
                                    BrandManufacturerModel brandManufacturerModel = new BrandManufacturerModel();
                                    try {
                                        JSONObject rec = jsonArrayItems.getJSONObject(i);
                                        brandManufacturerModel.setBarnd_manufacturer_id(rec.getString("ManufacturerID"));
                                        brandManufacturerModel.setBrand__manufacturer_part_number(rec.getString("Part_No"));
                                        brandManufacturerModel.setBrand_Parts_id(rec.getString("PartsID"));
                                        //brandManufacturerModel.setBrand_manufacturer_description(rec.getString("description"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    dismissProgressDialog();
                                    brandManufacturerModelList.add(brandManufacturerModel);

                                }
                                brandManufacturerAdapter = new BrandManufacturerAdapter(BrandManufacturerActivity.this, brandManufacturerModelList);
                                recyclerViewManufacturer.setAdapter(brandManufacturerAdapter);

                            } else {
                                /*String loginError = jsonObject.getJSONObject("response").getString("msg_for_user");*/
                                Toast.makeText(BrandManufacturerActivity.this, "No manufacturer exist", Toast.LENGTH_SHORT).show();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "ERROR_LOGIN: " + e.getMessage());
                        }
                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "error" + error);
                        Toast.makeText(BrandManufacturerActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
               /* params.put("brand_id", String.valueOf(brandID));
                params.put("page_no", String.valueOf(pageNo));*/
               if(ManIdFromCt!= null){
                   params.put("manid", ManIdFromCt);
                   params.put("man", UserData.ALPHABET_VALUE);
                   params.put("ct_id",UserData.CT_VALUE);
               }
               else{
                   params.put("manid", UserData.BRAND_VALUE_FOR_ALPHABET_MANUFACTURER);
                   params.put("man", UserData.ALPHABET_VALUE);
                   //params.put("ct_id",UserData.CT_VALUE);
               }
                Log.d(TAG, "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);

    }

    @OnClick(R.id.viewNextTextView)
    public void onViewClicked() {
        pageNo++;
        getManufacturerDetail();
    }
}
