package zwm.example.shubhangi.industrialservopart.model;

/**
 * Created by shubhangi on 1/16/2018.
 */

public class CartItemModel {
    private String cart_item_id;
    private String cart_item_title;
    private String cart_item_image;
    private String cart_item_price;
    private String cart_item_quantity;
    private String cart_item_updated_price;
    private String cart_item_SKU_number;


    public String getCart_item_id() {
        return cart_item_id;
    }

    public void setCart_item_id(String cart_item_id) {
        this.cart_item_id = cart_item_id;
    }

    public String getCart_item_title() {
        return cart_item_title;
    }

    public void setCart_item_title(String cart_item_title) {
        this.cart_item_title = cart_item_title;
    }

    public String getCart_item_image() {
        return cart_item_image;
    }

    public void setCart_item_image(String cart_item_image) {
        this.cart_item_image = cart_item_image;
    }

    public String getCart_item_price() {
        return cart_item_price;
    }

    public void setCart_item_price(String cart_item_price) {
        this.cart_item_price = cart_item_price;
    }

    public String getCart_item_quantity() {
        return cart_item_quantity;
    }

    public void setCart_item_quantity(String cart_item_quantity) {
        this.cart_item_quantity = cart_item_quantity;
    }

    public String getCart_item_updated_price() {
        return cart_item_updated_price;
    }

    public void setCart_item_updated_price(String cart_item_updated_price) {
        this.cart_item_updated_price = cart_item_updated_price;
    }

    public String getCart_item_SKU_number() {
        return cart_item_SKU_number;
    }

    public void setCart_item_SKU_number(String cart_item_SKU_number) {
        this.cart_item_SKU_number = cart_item_SKU_number;
    }
}
