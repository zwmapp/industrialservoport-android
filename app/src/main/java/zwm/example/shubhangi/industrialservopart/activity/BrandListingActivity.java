package zwm.example.shubhangi.industrialservopart.activity;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.adapter.AlphabeticListingAdapter;
import zwm.example.shubhangi.industrialservopart.adapter.BrandListingAdapter;
import zwm.example.shubhangi.industrialservopart.model.AlphaBeticModel;
import zwm.example.shubhangi.industrialservopart.model.BrandListingModel;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.util.UserData;
import zwm.example.shubhangi.industrialservopart.volleyparser.GetServiceCall;

/**
 * Created by shubhangi on 1/8/2018.
 */

public class BrandListingActivity extends BaseActivity {
    @BindView(R.id.tvToolbarText)
    TextView tvToolbarText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerViewBrandListing)
    RecyclerView recyclerViewBrandListing;
    List<BrandListingModel> brandListingModelList;
    BrandListingAdapter brandListingAdapter;

    //Servo Motor Listing
    @BindView(R.id.recyclerViewServoMotorRepairListing)
    RecyclerView recyclerViewServoMotorRepairListing;
/*
    List<AlphaBeticModel> alphaBeticModelList;
*/

    public String[] alphaBeticList = {"[0-9]", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    private AlphabeticListingAdapter alphabeticListingAdapter;
    int numberOfColumns = 8;
    String buttonNameServoMotor;
    String buttonNameIndustrialMonitorsRepair;
    //Servo Motor Listing

    int numberOfColumnsForBrandListings =2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_listing);
        ButterKnife.bind(this);

        Bundle b = getIntent().getExtras();
        buttonNameServoMotor = b.getString("buttonServoMotorRepair");
        UserData.SERVO_MOTOR_REPAIR_BUTTON = buttonNameServoMotor;
        Log.d(TAG, "buttonNameServoMotor -" + UserData.SERVO_MOTOR_REPAIR_BUTTON);

        Bundle b1 = getIntent().getExtras();
        buttonNameIndustrialMonitorsRepair = b1.getString("buttonIndustrialMonitorsRepair");
        UserData.INDUSTRIAL_MONITORS_REPAIR_BUTTON = buttonNameIndustrialMonitorsRepair;
        Log.d(TAG, "buttonNameIndustrialMonitorsRepair-" + UserData.INDUSTRIAL_MONITORS_REPAIR_BUTTON);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (buttonNameServoMotor != null) {
            tvToolbarText.setText("Servo Motors Repair");
        } else {
            tvToolbarText.setText("Industrial Monitors Repair");

        }
        //tvToolbarText.setText("Brands");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        recyclerViewBrandListing.setLayoutManager(new GridLayoutManager(this, numberOfColumnsForBrandListings));
        brandListingModelList = new ArrayList<>();
        recyclerViewBrandListing.setHasFixedSize(true);

        //Servo Motor Listing
        /*alphaBeticModelList = new ArrayList<>();
        AlphaBeticModel alphaBeticModel = new AlphaBeticModel();
        for (int i = 0; i < alphaBeticList.length; i++) {
            alphaBeticModel.setTitile(alphaBeticList.toString());
        }
        alphaBeticModelList.add(alphaBeticModel);*/
        recyclerViewServoMotorRepairListing.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        alphabeticListingAdapter = new AlphabeticListingAdapter(BrandListingActivity.this, alphaBeticList);
        recyclerViewServoMotorRepairListing.setAdapter(alphabeticListingAdapter);
        //Servo Motor Listing

        brandListingDetail();
    }

    private void brandListingDetail() {
        showProgressDialog("Loading..");
        new GetServiceCall(Urls.ALL_BRAND, GetServiceCall.TYPE_JSONOBJECT) {
            @Override
            public void response(String response) {
                Log.d(TAG, "response: " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.d(TAG, "jsonObject" + jsonObject);
                    JSONArray jsonA = jsonObject.optJSONArray("response");
                    Log.d(TAG, "jsonA" + jsonA);
                    for (int i = 0; i < jsonA.length(); i++) {

                        BrandListingModel brandListingModel = new BrandListingModel();
                        try {
                            JSONObject rec = jsonA.getJSONObject(i);
                            brandListingModel.setBarnd_id(rec.getString("ID"));
                            brandListingModel.setBrand_title(rec.getString("title"));
                            brandListingModel.setBrand_image(rec.getString("image"));
                            brandListingModel.setBrand_description(rec.getString("description"));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dismissProgressDialog();
                        brandListingModelList.add(brandListingModel);
                    }
                    brandListingAdapter = new BrandListingAdapter(BrandListingActivity.this, brandListingModelList);
                    recyclerViewBrandListing.setAdapter(brandListingAdapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                // dismissProgressDialog();
            }

            @Override
            public void error(VolleyError error, String errorMsg) {
                Log.d(TAG, "error: " + error.toString());
                Log.d(TAG, "errorMsg: " + errorMsg.toString());
                dismissProgressDialog();
            }
        }.call();
    }
}
