package zwm.example.shubhangi.industrialservopart.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.util.MyApplication;
import zwm.example.shubhangi.industrialservopart.util.PayPalConfig;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.util.UserData;

/**
 * Created by shubhangi on 1/17/2018.
 */

public class CheckOutActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvToolbarText)
    TextView tvToolbarText;
    @BindView(R.id.editTotalBill)
    TextView editTotalBill;
    @BindView(R.id.editPayableAmount)
    TextView editPayableAmount;

    @BindView(R.id.textViewAddress)
    TextView textViewAddress;
    @BindView(R.id.textViewUserDetails)
    TextView textViewUserDetails;
    @BindView(R.id.paymentLayout)
    LinearLayout paymentLayout;

    // paypal info
    String txnIdForPaypal, statusForPaypal;
    // paypal info

    //paypal
    private String paymentAmount;
    //Paypal intent request code to track onActivityResult method
    public static final int PAYPAL_REQUEST_CODE = 123;
    //Paypal Configuration Object
    private static PayPalConfiguration config = new PayPalConfiguration()
            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(PayPalConfig.PAYPAL_CLIENT_ID);

    //paypal
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);

        editTotalBill.setText("$" + UserData.CART_TOTAL_BILL);

        Float tempValue = Float.parseFloat("4.93");
        float totalPayablePriceForUser = Integer.parseInt(UserData.CART_TOTAL_BILL) + tempValue ;
        Log.d(TAG,"totalPayablePriceForUser" +totalPayablePriceForUser);
        paymentAmount = String.valueOf(totalPayablePriceForUser);

        editPayableAmount.setText("$" + String.valueOf(totalPayablePriceForUser));


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tvToolbarText.setText("Checkout");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getUserDetail();

        //paypal
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
        //paypal
    }


    @OnClick({R.id.editAddressDetail, R.id.buttonCheckout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.editAddressDetail:
                commonApi.openNewScreen(MyAccountActivity.class, null);
                break;
            case R.id.buttonCheckout:
                if(textViewAddress.equals("")){
                    Toast.makeText(this, "Fill your address", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    getPayment();

                }
               /* uploadCheckoutData();*/
              /* getPayment();*/
                break;
        }
    }

    private void getPayment() {
        //Getting the amount from editText
//        paymentAmount = UserData.CART_TOTAL_BILL;

        //Creating a paypalpayment
        PayPalPayment payment = new PayPalPayment(new BigDecimal(String.valueOf(paymentAmount)), "USD", "Total Bill",
                PayPalPayment.PAYMENT_INTENT_SALE);

        //Creating Paypal Payment activity intent
        Intent intent = new Intent(this, PaymentActivity.class);

        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        //Putting paypal payment to the intent
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        //Starting the intent activity for result
        //the request code will be used on the method onActivityResult
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //If the result is from paypal
        if (requestCode == PAYPAL_REQUEST_CODE) {

            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.i("paymentExample", paymentDetails);

                        JSONObject jsonO = new JSONObject(paymentDetails);
                        Log.d(TAG, "jsonO" + jsonO);
                        JSONObject jsonDetails = jsonO.getJSONObject("response");
                        txnIdForPaypal = jsonDetails.getString("id");
                        statusForPaypal = jsonDetails.getString("state");
                        Log.d(TAG, "txnId" + UserData.PAYMENT_TXN_ID);
                        uploadCheckoutData();

                        //Starting a new activity for the payment details and also putting the payment details with intent
                        startActivity(new Intent(this, ConfirmationActivity.class)
                                .putExtra("PaymentDetails", paymentDetails)
                                .putExtra("PaymentAmount", paymentAmount));

                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }
    private void uploadCheckoutData() {
        showProgressDialog("Please Wait..");
        StringRequest stringRequestNew = new StringRequest(Request.Method.POST, Urls.PLACE_ORDER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                       /* getPayment();*/
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                JSONObject jsonObjectBillingInfo = jsonObject.getJSONObject("items");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "ERROR_LOGIN: " + e.getMessage());
                        }
                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CheckOutActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", UserData.USER_ID);
                params.put("name", UserData.USER_FULL_NAME);
                params.put("username", UserData.USER_USERNAME);
                params.put("email", UserData.USER_EMAIL);
                params.put("phone", UserData.USER_PHONE);
                params.put("address", UserData.USER_ADDRESS);
                params.put("total", UserData.CART_TOTAL_BILL);
                params.put("payment_type", "paypal");
                params.put("payment_txn_id", txnIdForPaypal);
                params.put("payment_status", statusForPaypal);
                params.put("contactUs10", "01");
                Log.d(TAG, "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }


    private void getUserDetail() {
        showProgressDialog("Please Wait..");
        StringRequest stringRequestNew = new StringRequest(Request.Method.POST, Urls.USER_DETAIL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "response" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                JSONObject jsonObjectItems = jsonObject.getJSONObject("response");

                                UserData.USER_FULL_NAME = jsonObjectItems.getString("full_name");
                                UserData.USER_PHONE = jsonObjectItems.getString("phone_number");
                                UserData.USER_USERNAME = jsonObjectItems.getString("user_name");
                                UserData.USER_ADDRESS = jsonObjectItems.getString("address");
                                UserData.USER_EMAIL = jsonObjectItems.getString("email");

                                textViewAddress.setText(UserData.USER_ADDRESS);
                                textViewUserDetails.setText(UserData.USER_FULL_NAME +"\n"+
                                UserData.USER_EMAIL + "\n" +
                                UserData.USER_PHONE);

                               /* editor = getSharedPreferences("UserPref", 0).edit();
                                editor.putString("key_userid", UserData.USER_ID);
                                editor.putString("key_userUsername", UserData.USER_USERNAME );
                                editor.putString("key_userfullname",UserData.USER_FULL_NAME);
                                editor.putString("key_email", UserData.USER_EMAIL);
                                editor.putString("key_phone", UserData.USER_PHONE);
                                editor.putString("key_address", UserData.USER_ADDRESS);
                                editor.apply();*/


                              /*  etFullName.setText(UserData.USER_FULL_NAME);
                                etEmailAddress.setText(UserData.USER_EMAIL);
                                etUserName.setText(UserData.USER_USERNAME);
                                etPhone.setText(UserData.USER_PHONE);
                                etAddressLine1.setText(UserData.USER_ADDRESS);*/
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "ERROR_LOGIN: " + e.getMessage());
                        }
                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CheckOutActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_ID", UserData.USER_ID);
                Log.d(TAG, "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);
    }
}
