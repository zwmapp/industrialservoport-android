package zwm.example.shubhangi.industrialservopart.model;

/**
 * Created by shubhangi on 4/13/2018.
 */

public class MyOrderDetailsModel {
    private String ordered_details_product_id;
    private String ordered_details_product_title;
    private String ordered_details_product_image;
    private String ordered_details_product_price;
    private String ordered_details_product_quantity;

    public String getOrdered_details_product_id() {
        return ordered_details_product_id;
    }

    public void setOrdered_details_product_id(String ordered_details_product_id) {
        this.ordered_details_product_id = ordered_details_product_id;
    }

    public String getOrdered_details_product_title() {
        return ordered_details_product_title;
    }

    public void setOrdered_details_product_title(String ordered_details_product_title) {
        this.ordered_details_product_title = ordered_details_product_title;
    }

    public String getOrdered_details_product_image() {
        return ordered_details_product_image;
    }

    public void setOrdered_details_product_image(String ordered_details_product_image) {
        this.ordered_details_product_image = ordered_details_product_image;
    }

    public String getOrdered_details_product_price() {
        return ordered_details_product_price;
    }

    public void setOrdered_details_product_price(String ordered_details_product_price) {
        this.ordered_details_product_price = ordered_details_product_price;
    }

    public String getOrdered_details_product_quantity() {
        return ordered_details_product_quantity;
    }

    public void setOrdered_details_product_quantity(String ordered_details_product_quantity) {
        this.ordered_details_product_quantity = ordered_details_product_quantity;
    }
}
