package zwm.example.shubhangi.industrialservopart.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.activity.BaseActivity;
import zwm.example.shubhangi.industrialservopart.activity.EbaySubProductDetailDescriptionActivity;
import zwm.example.shubhangi.industrialservopart.model.TopRecommendedProductListModel;
import zwm.example.shubhangi.industrialservopart.util.MyApplication;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.util.UserData;

/**
 * Created by shubhangi on 1/4/2018.
 */

public class TopRecommendedProductAdapter extends RecyclerView.Adapter<TopRecommendedProductAdapter.ViewHolder> {

    List<TopRecommendedProductListModel> topRecommendedProductListModelList;
    TopRecommendedProductListModel topRecommendedProductListModel;
    private Context context;
    private ItemClickListener mClickListener;


    // data is passed into the constructor
    public TopRecommendedProductAdapter(Context context, List<TopRecommendedProductListModel> topRecommendedProductListModelList) {
        super();
        this.context = context;
        this.topRecommendedProductListModelList = topRecommendedProductListModelList;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.top_recommended_recyclerview_items, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        topRecommendedProductListModel = topRecommendedProductListModelList.get(position);

        holder.nameProduct.setText(topRecommendedProductListModel.getTitle());
        holder.nameProduct.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        holder.priceProduct.setText("$" + topRecommendedProductListModel.getPrice());
       /* holder.buttonProductView.setText("$"+topRecommendedProductListModel.getPrice());*/
        holder.buttonAddToCartTopRecomProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(UserData.LOGGED_IN) {
                    addToCartMethod(topRecommendedProductListModelList.get(position).getID());
                }
                else
                {
                    Toast.makeText(context, "Please log in to add item in cart.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        Glide.with(context)
                .load(topRecommendedProductListModel.getImage())
                .into(holder.imageProduct);
        holder.topRecommendedProductId = topRecommendedProductListModelList.get(position).getEbay_id();

    }

    // total number of cells
    @Override
    public int getItemCount() {
        return topRecommendedProductListModelList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView nameProduct, priceProduct;
        public ImageView imageProduct;
        public TextView buttonAddToCartTopRecomProduct;
        String topRecommendedProductId;
       /* public TextView buttonProductView;*/

        public ViewHolder(View itemView) {
            super(itemView);
            nameProduct = itemView.findViewById(R.id.nameProduct);
            priceProduct = itemView.findViewById(R.id.priceProduct);
            imageProduct = itemView.findViewById(R.id.imageProduct);
            buttonAddToCartTopRecomProduct = itemView.findViewById(R.id.buttonAddToCartTopRecomProduct);
            /*buttonProductView = itemView.findViewById(R.id.buttonProductView);*/
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            Intent in = new Intent(context, EbaySubProductDetailDescriptionActivity.class);
            Bundle b = new Bundle();
            b.putString("ebayId", topRecommendedProductId);
            Log.d("TAG", "ebayId" + topRecommendedProductId);
            in.putExtras(b);
            context.startActivity(in);
        }
    }

    // allows clicks events to be caught
    public void setClickListener(TopRecommendedProductAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    private void addToCartMethod(final String productID) {
        ((BaseActivity) context).showProgressDialog("Please Wait..");
        StringRequest stringRequestNew = new StringRequest(com.android.volley.Request.Method.POST, Urls.ADD_TO_CART,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        Log.d("TAG", "response" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                Toast.makeText(context, "Please check your cart", Toast.LENGTH_LONG).show();
                                JSONObject jsonObjectItemsAddToCart = jsonObject.getJSONObject("response");
                                BaseActivity activity = (BaseActivity) context;
                                BaseActivity.finalCartCount = jsonObjectItemsAddToCart.getInt("count_product");
                                activity.refreshCartCounter();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("TAG", "ERROR_LOGIN: " + e.getMessage());
                        }
                        ((BaseActivity) context).dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                        ((BaseActivity) context).dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", UserData.USER_ID);
                params.put("product_id", productID);
                params.put("qty", "1");
                Log.d("TAG", "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);
    }

}


