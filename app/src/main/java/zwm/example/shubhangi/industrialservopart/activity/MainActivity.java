package zwm.example.shubhangi.industrialservopart.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.adapter.BrandManufacturerAdapter;
import zwm.example.shubhangi.industrialservopart.adapter.ServicesListAdapter;
import zwm.example.shubhangi.industrialservopart.adapter.TopRecommendedProductAdapter;
import zwm.example.shubhangi.industrialservopart.alertdialog.DialogBoxFactory;
import zwm.example.shubhangi.industrialservopart.fragment.FragmentDrawer;
import zwm.example.shubhangi.industrialservopart.model.BrandManufacturerModel;
import zwm.example.shubhangi.industrialservopart.model.ServicesModel;
import zwm.example.shubhangi.industrialservopart.model.TopRecommendedProductListModel;
import zwm.example.shubhangi.industrialservopart.util.MyApplication;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.util.UserData;
import zwm.example.shubhangi.industrialservopart.volleyparser.GetServiceCall;

import static zwm.example.shubhangi.industrialservopart.util.UserData.clearUserData;

public class MainActivity extends BaseActivity implements FragmentDrawer.FragmentDrawerListener {

    @BindView(R.id.tvToolbarText)
    TextView tvToolbarText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.searchViewForProducts)
    SearchView searchViewForProducts;
    //services
    @BindView(R.id.recyclerViewServices)
    RecyclerView recyclerViewServices;
    List<ServicesModel> servicesModelList;
    ServicesListAdapter servicesListAdapter;

    // Product
    @BindView(R.id.recyclerViewProductsTopRecommended)
    RecyclerView recyclerViewProductsTopRecommended;
    List<TopRecommendedProductListModel> topRecommendedProductListModelList;
    int numberOfColumns = 2;
    TopRecommendedProductAdapter topRecommendedProductAdapter;

    // for navigation drawer
    private FragmentDrawer drawerFragment;

    String ButtonName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tvToolbarText.setText("Industrial Automation Repair");

        // for navigation drawer
        drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.setDrawerListener(this);
        // for navigation drawer

        //Product
        topRecommendedProductListModelList = new ArrayList<>();
        recyclerViewProductsTopRecommended.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        recyclerViewProductsTopRecommended.setHasFixedSize(true);
        recyclerViewProductsTopRecommended.setNestedScrollingEnabled(true);
        //Product

        //service
        servicesModelList = new ArrayList<>();
        recyclerViewServices.setHasFixedSize(true);
        recyclerViewProductsTopRecommended.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerViewServices.setNestedScrollingEnabled(true);
        //service

        // Search View for product searching by part list
        searchViewForProducts.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String s) {
                Bundle b = new Bundle();
                b.putString("part_number_for_search",s);
                commonApi.openNewScreen(SearchResultActivity.class,b);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        // Search View for product searching by part list

        topRecommendedProductDetail();

    }

    private void topRecommendedProductDetail() {
        showProgressDialog("Loading..");
        new GetServiceCall(Urls.HOME_PAGE, GetServiceCall.TYPE_JSONOBJECT) {
            @Override
            public void response(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonO = jsonObject.optJSONObject("response");
                    JSONArray jsonArrayproducts = jsonO.getJSONArray("top_recomended");
                    JSONArray jsonArrayServices = jsonO.getJSONArray("services");

                    for (int i = 0; i < 4; i++) {
                        TopRecommendedProductListModel topRecommendedProductListModel = new TopRecommendedProductListModel();
                        try {
                            JSONObject rec = jsonArrayproducts.getJSONObject(i);
                            topRecommendedProductListModel.setID(rec.getString("ID"));
                            topRecommendedProductListModel.setEbay_id(rec.getString("ebay_id"));
                            topRecommendedProductListModel.setTitle(rec.getString("title"));
                            topRecommendedProductListModel.setPrice(rec.getString("price"));
                            topRecommendedProductListModel.setImage(rec.getString("image"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dismissProgressDialog();
                        topRecommendedProductListModelList.add(topRecommendedProductListModel);
                    }
                    topRecommendedProductAdapter = new TopRecommendedProductAdapter(MainActivity.this, topRecommendedProductListModelList);
                    recyclerViewProductsTopRecommended.setAdapter(topRecommendedProductAdapter);


                    for (int j = 0; j < jsonArrayServices.length(); j++) {
                        ServicesModel servicesModel = new ServicesModel();
                        try {
                            JSONObject rec1 = jsonArrayServices.getJSONObject(j);
                            servicesModel.setTitle(rec1.getString("title"));
                            servicesModel.setDescription(rec1.getString("description"));
                            servicesModel.setImage(rec1.getString("image"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dismissProgressDialog();
                        servicesModelList.add(servicesModel);
                    }
                    servicesListAdapter = new ServicesListAdapter(MainActivity.this, servicesModelList);
                    recyclerViewServices.setAdapter(servicesListAdapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                // dismissProgressDialog();
            }

            @Override
            public void error(VolleyError error, String errorMsg) {
                Log.d(TAG, "error: " + error.toString());
                Log.d(TAG, "errorMsg: " + errorMsg.toString());
                dismissProgressDialog();
            }
        }.call();
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    public void displayView(int position) {
        switch (position) {
            case 0:
                //home
                break;
            case 1:
                //servo motor repair
                ButtonName = "buttonServoMotorRepair";
               /* Toast.makeText(this, "" + ButtonName, Toast.LENGTH_SHORT).show();*/
                Intent in = new Intent(this, BrandListingActivity.class);
                Bundle b = new Bundle();
                b.putString("buttonServoMotorRepair", ButtonName);
                in.putExtras(b);
                startActivity(in);
                // commonApi.openNewScreen(BrandListingActivity.class, null);
                break;
            case 2:
                // industrial monitors repair
                ButtonName = "buttonIndustrialMonitorsRepair";
              /*  Toast.makeText(this, "" + ButtonName, Toast.LENGTH_SHORT).show();*/

                Intent in1 = new Intent(this, BrandListingActivity.class);
                Bundle b1 = new Bundle();
                b1.putString("buttonIndustrialMonitorsRepair", ButtonName);
                in1.putExtras(b1);
                startActivity(in1);
                // commonApi.openNewScreen(BrandListingActivity.class, null);
                break;
          /*  case 3:
                //employment
                break;*/

            case 3:
                //my account
                if (!UserData.LOGGED_IN) {
                    new DialogBoxFactory("Error!!", "Sorry, you are not logged in. Please log in to continue.", MainActivity.this, DialogBoxFactory.TYPE_SINGLE_BUTTON, "OK") {
                        @Override
                        public void positiveClick(DialogInterface dialog, int id) {
                            commonApi.openNewScreen(LoginActivity.class, null);
                            dialog.dismiss();
                        }

                        @Override
                        public void negativeClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    }.call();
                    break;
                }
                commonApi.openNewScreen(MyAccountActivity.class, null);
                break;
            case 4:
                //my order
                if (!UserData.LOGGED_IN) {
                    new DialogBoxFactory("Error!!", "Sorry, you are not logged in. Please log in to continue.", MainActivity.this, DialogBoxFactory.TYPE_SINGLE_BUTTON, "OK") {
                        @Override
                        public void positiveClick(DialogInterface dialog, int id) {
                            commonApi.openNewScreen(LoginActivity.class, null);
                            dialog.dismiss();
                        }

                        @Override
                        public void negativeClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    }.call();
                    break;
                }
                commonApi.openNewScreen(MyOrderActivity.class, null);
                break;
            case 5:
                commonApi.openNewScreen(ShippingAndPaymentActivity.class, null);
                break;
            case 6:
                //login/logout
                if (!UserData.LOGGED_IN)
                    commonApi.openNewScreen(LoginActivity.class, null);
                else
                    showAlert(0, "Log Out", "Are you sure you want to logout?");
                break;
            //commonApi.openNewScreen(LoginActivity.class, null);

            default:
                break;

        }
    }

    public void showAlert(final int flag, String title, String message) {
        AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this, R.style.AlertDialog);
        if (flag != 1) {
            adb.setTitle(title);
        }
        adb.setMessage(message);
        adb.setCancelable(false);

        adb.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (flag == 0) performLogOut();
                else MainActivity.super.onBackPressed();
            }
        });

        adb.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog ad = adb.create();
        ad.show();
    }

    public void performLogOut() {
        clearUserData(MainActivity.this);
        new DialogBoxFactory("Thank you", "You have successfully logged out", MainActivity.this, DialogBoxFactory.TYPE_SINGLE_BUTTON, "OK") {
            @Override
            public void positiveClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                MainActivity.this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                dialog.dismiss();
            }

            @Override
            public void negativeClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        }.call();
    }

    @OnClick({R.id.buttonEBayCategories, R.id.buttonServoMotorRepair, R.id.buttonIndustrialMonitorsRepair,
            R.id.viewMoreTextView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonEBayCategories:
                commonApi.openNewScreen(EBayCategoriesActivity.class, null);
                break;
            case R.id.buttonServoMotorRepair:
                ButtonName = "buttonServoMotorRepair";
               /* Toast.makeText(this, "" + ButtonName, Toast.LENGTH_SHORT).show();*/
                Intent in = new Intent(this, BrandListingActivity.class);
                Bundle b = new Bundle();
                b.putString("buttonServoMotorRepair", ButtonName);
                in.putExtras(b);
                startActivity(in);
//
//                commonApi.openNewScreen(BrandListingActivity.class, null);
                break;
            case R.id.buttonIndustrialMonitorsRepair:
                ButtonName = "buttonIndustrialMonitorsRepair";
              /*  Toast.makeText(this, "" + ButtonName, Toast.LENGTH_SHORT).show();*/

                Intent in1 = new Intent(this, BrandListingActivity.class);
                Bundle b1 = new Bundle();
                b1.putString("buttonIndustrialMonitorsRepair", ButtonName);
                in1.putExtras(b1);
                startActivity(in1);
                /*
                commonApi.openNewScreen(BrandListingActivity.class, null);*/
                break;
            case R.id.viewMoreTextView:
                commonApi.openNewScreen(TopRecommendedAllProduct.class, null);
        }
    }
}
