package zwm.example.shubhangi.industrialservopart.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.drakeet.materialdialog.MaterialDialog;
import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.util.Constant;
import zwm.example.shubhangi.industrialservopart.util.MyApplication;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.util.UserData;

/**
 * Created by shubhangi on 12/6/2017.
 */

public class LoginActivity extends BaseActivity {

    @BindView(R.id.etLoginEmail)
    EditText etLoginEmail;
    @BindView(R.id.etLoginPassword)
    EditText etLoginPassword;
    @BindView(R.id.loginLayout)
    LinearLayout loginLayout;

    //forgot Password
    MaterialDialog materialDialog;
    EditText forgotPasswordEmail;
    //forgot Password
    private SharedPreferences.Editor editor;


    private String errorMsg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        errorMsg = "";

    }

    @OnClick({R.id.textViewForgotPassword, R.id.buttonLogin, R.id.textViewSignUp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.textViewForgotPassword:
                openDialogMethod();
                break;
            case R.id.buttonLogin:
                commonApi.errorMsg = "";
                errorMsg = commonApi.checkRequiredFields(loginLayout);
                if (errorMsg.equals("")) {
                    uploadLoginData();
                } else {
                    commonApi.showSnackBar(loginLayout, "" + errorMsg, 1);
                    return;
                }
                break;
            case R.id.textViewSignUp:
                commonApi.openNewScreen(SignUpActivity.class, null);
                break;
        }
    }

    private void uploadLoginData() {
        if (!commonApi.checkEmailValidation(etLoginEmail.getText().toString().trim())) {
            commonApi.showSnackBar(loginLayout, Constant.EMAIL_VALIDATION + errorMsg, 1);
            return;
        }
        showProgressDialog("Please wait..");
        StringRequest stringRequestNew = new StringRequest(Request.Method.POST, Urls.LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "responseS" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                JSONObject jsonObjectItemsLogin = jsonObject.getJSONObject("response");
                                UserData.USER_ID = jsonObjectItemsLogin.getString("user_ID");
                                UserData.USER_FULL_NAME = jsonObjectItemsLogin.getString("full_name");
                                UserData.USER_EMAIL = jsonObjectItemsLogin.getString("email");
                                UserData.LOGGED_IN = true;


                                editor = getSharedPreferences("UserPref", 0).edit();
                                editor.putString("key_userid", UserData.USER_ID);
                                editor.putString("key_userfullname", UserData.USER_FULL_NAME);
                                editor.putString("key_email", UserData.USER_EMAIL);
                                editor.putBoolean("key_loggedIn", UserData.LOGGED_IN);

                                editor.apply();
                                LoginActivity.this.finish();
                               // commonApi.openNewScreen(MainActivity.class, null);
                            }
                            else
                            {
                                String loginError = jsonObject.getJSONObject("response").getString("msg_for_user");
                                Toast.makeText(LoginActivity.this, loginError, Toast.LENGTH_SHORT).show();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "ERROR_LOGIN: " + e.getMessage());
                        }
                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG,"error" +error);
                        Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", etLoginEmail.getText().toString().trim());
                params.put("password", etLoginPassword.getText().toString().trim());
                params.put("contactUs01","1");
                Log.d(TAG, "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);
    }

    private void openDialogMethod() {
        View view1 = LayoutInflater.from(this).inflate(R.layout.forgot_password_email_dialog, null);
        materialDialog = new MaterialDialog(this).setContentView(view1);
        ImageView imageCloseIcon = (ImageView) view1.findViewById(R.id.imageCloseIcon);
        forgotPasswordEmail = (EditText) view1.findViewById(R.id.forgotPasswordEmail);
        Button buttonEmailSubmit = (Button) view1.findViewById(R.id.buttonEmailSubmit);

        imageCloseIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                materialDialog.dismiss();
            }
        });

        buttonEmailSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!commonApi.checkEmailValidation(forgotPasswordEmail.getText().toString().trim())) {
                    Toast.makeText(LoginActivity.this, Constant.EMAIL_VALIDATION, Toast.LENGTH_SHORT).show();
                    return;
                }
                showProgressDialog("Please wait..");
                StringRequest stringRequestNew = new StringRequest(Request.Method.POST, Urls.FORGOT_PASSWORD,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d(TAG, "responseS" + response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                        String responseMsg = jsonObject.getJSONObject("response").getString("msg_for_user");
                                        Toast.makeText(LoginActivity.this, response, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.e(TAG, "ERROR_LOGIN: " + e.getMessage());
                                }
                                dismissProgressDialog();
                                materialDialog.dismiss();
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                                dismissProgressDialog();
                                materialDialog.dismiss();

                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("email", forgotPasswordEmail.getText().toString().trim());
                        params.put("contactUs010","1");
                        Log.d(TAG, "params" + params);
                        return params;
                    }
                };
                MyApplication.getInstance().addToRequestQueue(stringRequestNew);

            }
        });
        materialDialog.show();
    }

    /*private void forgotPasswordMethod() {
        showProgressDialog("Please wait..");
        StringRequest stringRequestNew = new StringRequest(Request.Method.POST, Urls.LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "responseS" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                                //String responseMsg = jsonObject.getJSONObject("response").getString("msg_for_user");
                                Toast.makeText(LoginActivity.this, response, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "ERROR_LOGIN: " + e.getMessage());
                        }
                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", forgotPasswordEmail.getText().toString().trim());
                params.put("contactUs010","1");
                Log.d(TAG, "params" + params);
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(stringRequestNew);
    }*/
}
