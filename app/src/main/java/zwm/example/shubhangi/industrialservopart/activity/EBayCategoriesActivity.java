package zwm.example.shubhangi.industrialservopart.activity;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.adapter.EbayCategoriesAdapter;
import zwm.example.shubhangi.industrialservopart.model.EbayCategoriesModel;
import zwm.example.shubhangi.industrialservopart.util.Urls;
import zwm.example.shubhangi.industrialservopart.volleyparser.GetServiceCall;

/**
 * Created by shubhangi on 1/5/2018.
 */

public class EBayCategoriesActivity extends BaseActivity {

    @BindView(R.id.tvToolbarText)
    TextView tvToolbarText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerViewEBayCategories)
    RecyclerView recyclerViewEBayCategories;
    List<EbayCategoriesModel> ebayCategoriesModelList;
    EbayCategoriesAdapter ebayCategoriesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ebay_categories);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tvToolbarText.setText("E-Bay Categories");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        ebayCategoriesModelList = new ArrayList<>();
        recyclerViewEBayCategories.setHasFixedSize(true);

        eBayCategoriesDetail();
    }

    private void eBayCategoriesDetail() {
        showProgressDialog("Loading..");
        new GetServiceCall(Urls.EBAY_CATEGORIES, GetServiceCall.TYPE_JSONOBJECT) {
            @Override
            public void response(String response) {
                Log.d(TAG, "response: " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.d(TAG, "jsonObject" + jsonObject);
                    JSONArray jsonA = jsonObject.optJSONArray("response");
                    Log.d(TAG, "jsonA" + jsonA);
                    for (int i = 0; i < jsonA.length(); i++) {

                        EbayCategoriesModel ebayCategoriesModel = new EbayCategoriesModel();
                        try {
                            JSONObject rec = jsonA.getJSONObject(i);
                            ebayCategoriesModel.setEbay_product_title(rec.getString("title"));
                            ebayCategoriesModel.setEbay_product_no_of_products(rec.getString("no_of_products"));
                            ebayCategoriesModel.setEbay_product_id(rec.getString("ID"));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dismissProgressDialog();
                        ebayCategoriesModelList.add(ebayCategoriesModel);
                    }
                    ebayCategoriesAdapter = new EbayCategoriesAdapter(EBayCategoriesActivity.this, ebayCategoriesModelList);
                    recyclerViewEBayCategories.setAdapter(ebayCategoriesAdapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                // dismissProgressDialog();
            }

            @Override
            public void error(VolleyError error, String errorMsg) {
                Log.d(TAG, "error: " + error.toString());
                Log.d(TAG, "errorMsg: " + errorMsg.toString());
                dismissProgressDialog();
            }
        }.call();
    }
}
