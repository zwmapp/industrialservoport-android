package zwm.example.shubhangi.industrialservopart.util;

/**
 * Created by shubhangi on 5/15/2018.
 */

public interface CartUpdateListener {

    public abstract void updateCartValue(String cartTotalPrice);
}
