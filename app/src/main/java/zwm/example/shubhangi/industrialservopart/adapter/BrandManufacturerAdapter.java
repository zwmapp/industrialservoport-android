package zwm.example.shubhangi.industrialservopart.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import zwm.example.shubhangi.industrialservopart.R;
import zwm.example.shubhangi.industrialservopart.activity.ManufacturerDetailActivity;
import zwm.example.shubhangi.industrialservopart.model.BrandManufacturerModel;

/**
 * Created by shubhangi on 1/9/2018.
 */

public class BrandManufacturerAdapter extends RecyclerView.Adapter<BrandManufacturerAdapter.ViewHolder> {

    List<BrandManufacturerModel> brandManufacturerModelList;
    BrandManufacturerModel brandManufacturerModel;
    private Context context;

    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public BrandManufacturerAdapter(Context context, List<BrandManufacturerModel> brandManufacturerModelList) {
        super();
        this.context = context;
        this.brandManufacturerModelList = brandManufacturerModelList;
    }

    // inflates the cell layout from xml when needed
    @Override
    public BrandManufacturerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.brand_manufacturer_recyclerview_items, parent, false);
        return new BrandManufacturerAdapter.ViewHolder(view);
    }

    // binds the data to the textView in each cell
    @Override
    public void onBindViewHolder(BrandManufacturerAdapter.ViewHolder holder, int position) {
        brandManufacturerModel = brandManufacturerModelList.get(position);

        holder.brandManufacturerPartNumber.setText(brandManufacturerModel.getBrand__manufacturer_part_number());
        holder.brandManufacturerDescription.setText(brandManufacturerModel.getBrand_manufacturer_description());
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return brandManufacturerModelList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView brandManufacturerPartNumber, brandManufacturerDescription;

        public ViewHolder(View itemView) {
            super(itemView);
            brandManufacturerPartNumber = itemView.findViewById(R.id.brandManufacturerPartNumber);
            brandManufacturerDescription = itemView.findViewById(R.id.brandManufacturerDescription);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            int moreProductTemp = getAdapterPosition();
            Intent in = new Intent(context, ManufacturerDetailActivity.class);
            in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle b = new Bundle();
            b.putString("manIdFORCT", brandManufacturerModelList.get(getAdapterPosition()).getBarnd_manufacturer_id());
            b.putString("manufacturerPartsID", brandManufacturerModelList.get(getAdapterPosition()).getBrand_Parts_id());
            in.putExtras(b);
            context.startActivity(in);
        }
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}





